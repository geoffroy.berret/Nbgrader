exercices du cours M2 Outils Informatiques pour la mécanique

# attention purge *py et *dati
find . -name "*py"
find . -name "*dat"
# notation
# liste etudiant, ajout etudiant, automatic grading
liste_etudiant NOM_TP
add_student.sh
nbgrader autograde "EF0_FormulationFaible"
# sortie notes db
nbgrader export 
# bilan notes (avec note max 20)
../bin/bilan_notes.py -M 20 EF0_FormulationFaible notes1.csv
# feedback
nbgrader feedback TPI2_CalculGeometrique
sudo python3 ~/bin/return_feedback.py EF0_FormulationFaible

