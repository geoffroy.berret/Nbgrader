import numpy as np
from Point import *
# classe Triangle
class Triangle(object):
    """creation d'un triangle dans l'espace cartesien"""
    def __init__(self,P1,P2,P3):
        ''' initialisation '''
        # attention on fait une copie des points !!!
        self.Pts = [P1.copy(),P2.copy(),P3.copy()]
        return
    def __str__(self):
        '''conversion chaine pour affichage'''
        return "Triangle:(%s,%s,%s)"%(self.Pts[0],self.Pts[1],self.Pts[2]) 
    def __eq__(self,T):
        '''test si le triangle est confondu avec T'''
        ## BEGIN SOLUTION
        Eq = True
        for i in range(3):
            # test si Pt i est confondu avec un pts de T
            res = False
            for j in range(3):
                res = res or self.Pts[i] == T.Pts[j]
            Eq = res
            if not Eq : break
        return Eq
        ## END SOLUTION
    def barycentre(self):
        '''calcul le point barycentre du triangle'''
        ## BEGIN SOLUTION
        x = 0
        y = 0
        for i in range(3):
            x = x + self.Pts[i].x
            y = y + self.Pts[i].y
        return Point(x/3.,y/3.)
        ## END SOLUTION
    def perimetre(self):
        '''calcul perimetre du triangle'''
        ## BEGIN SOLUTION
        c1 = distance(self.Pts[0],self.Pts[1])
        c2 = distance(self.Pts[1],self.Pts[2])
        c3 = distance(self.Pts[2],self.Pts[0])
        return c1+c2+c3
        ## END SOLUTION
    def surface(self):
        '''calcul la surface du triangle'''
        ## BEGIN SOLUTION
        aire = produit_vect(self.Pts[0],self.Pts[1],self.Pts[2])
        return 0.5*np.abs(aire)
        ## END SOLUTION
    def coordbary(self,P):
        ## BEGIN SOLUTION
        aire = produit_vect(self.Pts[0],self.Pts[1],self.Pts[2])
        s1 = produit_vect(P,self.Pts[1],self.Pts[2])
        s2 = produit_vect(self.Pts[0],P,self.Pts[2])
        l1 = s1/aire
        l2 = s2/aire
        l3 = 1 - l1 - l2
        ## END SOLUTION
        return l1,l2,l3
    def inside(self,P):
        '''test si le point P est à l intérieur du triangle'''
        ## BEGIN SOLUTION
        l1,l2,l3 = self.coordbary(P)
        In01 = lambda l: (l > -EPS_GEO) and (l < 1.+EPS_GEO)
        return In01(l1) and In01(l2) and In01(l3) 
        ## END SOLUTION
    def translation(self,dx,dy):
        '''translation du triangle de dx,dy'''
        ## BEGIN SOLUTION
        for i in range(3):
            self.Pts[i].translation(dx,dy)
        return
        ## END SOLUTION
    def rotation(self,O,alpha):
        """rotation du triangle de alpha autour de O"""
        ## BEGIN SOLUTION
        for i in range(3):
            self.Pts[i].rotation(O,alpha)
        return
        ## END SOLUTION
    def rayon(self):
        '''calcul le rayon du cercle contenant le triangle et centré au barycentre'''
        ## BEGIN SOLUTION
        B = self.barycentre()
        r0 = distance(B,self.Pts[0])
        r1 = distance(B,self.Pts[1])
        r2 = distance(B,self.Pts[2])
        return max(r0,r1,r2)
        ## END SOLUTION
    def intersection(self,T):
        '''détermine si il y a une intersection non nulle avec le traingle T'''
        ## BEGIN SOLUTION
        r1 = self.rayon()
        B1 = self.barycentre()
        r2 = T.rayon()
        B2 = T.barycentre()
        # test si les 2 traingles sont disjoints
        if distance(B1,B2) > r1+r2 :
            return False
        # determine le plus petit T1 des 2 traingles T1,T2
        T1 = self
        T2 = T
        if r2 < r1 :
            T1 = T
            T2 = self
        # test intersection des cotés de T1 avec T2
        CPts = [T1.Pts[0],T1.Pts[1],T1.Pts[2],T1.Pts[0]]
        In01 = lambda l: (l > -EPS_GEO) and (l < 1.+EPS_GEO)
        res = False
        la = [0]*3
        lb = [0]*3
        for i in range(3):
            # cote A,B du triangle T1
            A = CPts[i]
            la[0],la[1],la[2] = T2.coordbary(A)
            # test si A dans T2
            res = In01(la[0]) and In01(la[1]) and In01(la[2])
            if res:
                return True
            # 2nd point
            B = CPts[i+1]
            lb[0],lb[1],lb[2] = T2.coordbary(B)
            # calcul intersction avec les cotés de T2
            for j in range(3):
                if np.abs(lb[j]-la[j]) < EPS_GEO:
                    continue
                # intersection avec la droite
                alpha = lb[j]/(lb[j]-la[j])
                # test si intersction dans le segment AB
                if In01(alpha):
                    l1 = alpha*la[0] + (1-alpha)*lb[0]
                    l2 = alpha*la[1] + (1-alpha)*lb[1]
                    l3 = alpha*la[2] + (1-alpha)*lb[2]
                    # test si intersection dans T2
                    res = In01(l1) and In01(l2) and In01(l3)
                    if res :
                        return True
        # pas d'intersection
        return False
        ## END SOLUTION
    def plot(self,col='g'):
        '''tracer du triangle avec remplissage'''
        ## BEGIN SOLUTION
        X=[self.Pts[i].x for i in range(3)]
        Y=[self.Pts[i].y for i in range(3)]
        plt.fill(X,Y,col)
        return
        ## END SOLUTION
