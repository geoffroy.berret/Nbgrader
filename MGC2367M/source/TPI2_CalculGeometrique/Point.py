import numpy as np
import matplotlib.pyplot as plt
# precision des calcul geometrique
EPS_GEO = 1.e-5
# classe Point
class Point(object):
    """création d'un point dans l'espace cartesien"""
    def __init__(self,x,y):
        ''' initialisation '''
        self.x = x
        self.y = y
        return
    def copy(self):
        '''renvoie une copie du point'''
        return Point(self.x,self.y)
    def __str__(self):
        '''conversion chaine pour affichage'''
        return "Point:(%s,%s)"%(self.x,self.y) 
    def __eq__(self,P):
        ''' test si le point est confondu avec P a epsilon pres'''
        ## BEGIN SOLUTION
        dist = distance(self,P)
        return dist < EPS_GEO
        ## END SOLUTION
    def rayon(self):
        '''calcul le rayon du point / origine'''
        ## BEGIN SOLUTION
        r = np.sqrt(self.x**2+self.y**2)
        return r
        ## END SOLUTION
    def angle(self):
        '''calcul angle en degré de -180 a 180'''
        ## BEGIN SOLUTION
        alpha = np.angle(self.x+1j*self.y,deg=True)
        return alpha
        ## END SOLUTION
    def distance(self,P):
        '''calcul la distance au point P'''
        ## BEGIN SOLUTION
        d = distance(self,P)
        return d
        ## END SOLUTION 
    def rotation(self,O,beta):
        '''rotation du point par rapport au point O avec angle beta (en degré)'''
        ## BEGIN SOLUTION
        OP = Point(self.x-O.x,self.y-O.y)
        r = OP.rayon()
        alpha = (OP.angle() + beta)*np.pi/180.
        self.x = r*np.cos(alpha) + O.x
        self.y = r*np.sin(alpha) + O.y
        return
        ## END SOLUTION 
    def translation(self,dx,dy):
        '''translation du point de dx,dy'''
        ## BEGIN SOLUTION
        self.x += dx
        self.y += dy
        return
        ## END SOLUTION 
    def polaire(self):
        '''coordonnées polaire r,theta (en radian) du point'''
        ## BEGIN SOLUTION
        r = self.rayon()
        theta = np.angle(self.x+1j*self.y,deg=False)
        return r,theta
        ## END SOLUTION
    def plot(self):
        '''tracer du point'''
        ## BEGIN SOLUTION
        plt.plot([self.x],[self.y],'o')
        return
        ## END SOLUTION
#
# Fonctions utiles
#
# distance entre 2 points
def distance(P1,P2):
    """ calcul distance entre 2 points"""
    ## BEGIN SOLUTION
    d=np.sqrt((P1.x-P2.x)**2+(P1.y-P2.y)**2)
    return d
    ## END SOLUTION
# produit vectoriel AB x AC
def produit_vect(A,B,C):
    """ calcul produit vectoriel AB.AC (cpste suivant z)"""
    ## BEGIN SOLUTION
    AB=Point(B.x-A.x,B.y-A.y)
    AC=Point(C.x-A.x,C.y-A.y)
    prod = AB.x * AC.y - AB.y * AC.x
    return prod
    ## END SOLUTION