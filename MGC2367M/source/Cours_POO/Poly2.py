"""
classe de manipulation de polynomes de degre 2
"""
from math import *
class Poly2(object) :
    """Polynome du second degre."""
    def __init__(self,coefa,coefb,coefc) :
        """Construit un polynome a partir des coefficients a,b,c."""
        self.a = coefa
        self.b = coefb
        self.c = coefc
    def __str__(self) :
        """Chaine d'affichage du polynome."""
        s = "{0.a}x^2 + {0.b}x + {0.c}".format(self)
        return s
    def discriminant(self) :
        """Retourne le discriminant ("delta") du polynome."""
        return self.b**2 - (4*self.a*self.c)
    def racines(self) :
        """ Retourne un tuple contenant les racines reelles du polynome.
        Le tuple peut etre vide, contenir une racine, ou contenir deux racines.
        """
        delta = self.discriminant()
        if delta < 0:
            return ()
        elif delta == 0:
            return (-self.b / (2*self.a), )
        else:
            x1 = (-self.b - sqrt(delta))/(2*self.a)
            x2 = (-self.b + sqrt(delta))/(2*self.a)
            return (x1,x2)
    def calcul(self,x) :
        """Calcul du polynome pour valeur x."""
        return self.a*x**2 + self.b*x + self.c
    def __call__(self,x) :
        """Utilisation du polynome comme une fonction."""
        return self.calcul(x)
    def __add__(self,other) :
        """Ajout d'une valeur a la valeur du polynome."""
        if isinstance(other,Poly2) :
            return Poly2(self.a+other.a,self.b+other.b,self.c+other.c)
        else : # On assume que c'est un nombre...
            return Poly2(self.a,self.b,self.c+other)
    def __mul__(self,other) :
        """Multiplication de la valeur du polynome par une valeur."""
        # On assume que c'est un nombre...
        return Poly2(self.a*other,self.b*other,self.c*other)
# fin classe Poly2
