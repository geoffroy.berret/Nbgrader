# -*- coding: utf-8 -*-
# auteur NOM:xxxxx  PRENOM:yyyyy   NUMERO_ETUDIANT:zzzzz
# canvas programme Python
# bibliotheque
import numpy as np
import matplotlib.pyplot as plt
# consigne: 
#  - programmation fonction 
#    bien spécifier les arguments et la valeur de retour
#  - verification
#    a la fin du fichier dans la section :
#         if __name__ == '__main__'
#    on appelle la fonction avec un jeux d'arguments pour vérifier

# implementation
def lecture(fichier):
    '''lit les données dans fichier et renvoie les tableaux T,X,Y'''
    print("\tATTENTION: fonction non implémentée")
    return

def tri_bulles(T,X,Y):
    '''tri les données sur place (par rapport a T): les tableaux T,X,Y sont donc modifiés'''
    print("\tATTENTION: fonction non implémentée")
    return

def spectre(T,U):
    '''calcul et trace le spectre de U echantillonné en T'''
    print("\tATTENTION: fonction non implémentée")
    return

def analyse_spectral(T,U):
    '''calcul mode fondamentale du signal U échantillonné en T '''
    print("\tATTENTION: fonction non implémentée")
    return

def ecriture(fichier,T,X,Y):
    ''' Ecrire dans fichier de T,X,Y avec le format définit'''
    print("\tATTENTION: fonction non implémentée")
    return

    
# test de vérification
if __name__ == '__main__':
    print("verification lecture")
    lecture("mon_fichier.dat")

