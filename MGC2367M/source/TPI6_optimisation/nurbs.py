"""
Generates airfoil using NURBS. 
Original Paper: http://eprints.soton.ac.uk/50031/1/Sobe07.pdf
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt 

class NURBS():
        """
parametrisation d'un profil avec des NURBS et 2 points de contrôle:
 - extrado (upper side)
spline 2 pts A=[0,0] avec une tangente verticale intensité ta_u et B=[1,0] tangente d'angle alpha_b+alpha_c d'intensité tb_u

 - intrado (lower side)
idem avec ta_l et tb_l et l'angle alpha_c (en degre)

"""
        def __init__(self,k):
                """Takes a dictionary k of coefficients to define NURBS airfoil. 
                Coefficient names: ta_u,ta_l,tb_u,tb_l,alpha_b,alpha_c """

                self.k = k 

                self.ta_u = k['ta_u']
                self.ta_l = k['ta_l']
                self.tb_u = k['tb_u']
                self.tb_l = k['tb_l']
                self.alpha_b = k['alpha_b']
                self.alpha_c = k['alpha_c']
                return

        def __str__(self):
                '''affichage '''
                chaine="nurbs ta_u={} ta_l={} tb_u={} tb_l={} alpha_b={} alpha_c={}".\
                        format(self.ta_u,self.ta_l,self.tb_u,self.tb_u,self.alpha_b,self.alpha_c)
                return chaine

        def spline(self,N=100):
                """
                Calculates the spline interpolation for the airfoil params specified
                return the points on the lower and upper side: Xl,Yl,Xu,Yu
                """
                u = np.linspace(0,1,N)
                x_u = []
                y_u = []
                x_l = []
                y_l = []
                temp_var1 = np.array([[1,0,0,0],[0,0,1,0],[-3,3,-2,-1],[2,-2,1,1]])
                A = [0,0]
                B = [1,0]
                ta_u = self.ta_u
                ta_l = self.ta_l
                tb_u = self.tb_u
                tb_l = self.tb_l 
                alpha_b = self.alpha_b
                alpha_c = self.alpha_c

                #initialize end tangent magnitudes and directions
                TA_u = [(ta_u*np.cos(-np.pi/2)),ta_u*abs(np.sin(-np.pi/2))]
                TB_u = [(tb_u*np.cos(-((alpha_c+alpha_b)*np.pi/180))),(tb_u*np.sin(-((alpha_b+alpha_c)*np.pi/180)))] 
                TA_l = [(ta_l*np.cos(-np.pi/2)),ta_l*(np.sin(-np.pi/2))]
                TB_l = [(tb_l*np.cos(-((alpha_c)*np.pi/180))),(tb_l*np.sin(-((alpha_c)*np.pi/180)))] 

                #calculate (x,y) for the upper curve of the airfoil
                for j in range(2):
                        temp_var =  np.array([[A[j]],[B[j]],[TA_u[j]],[TB_u[j]]]) #control points for x and y coords
                        temp_var_coord = np.dot(temp_var1,temp_var)
                        for i in range(len(u)):
                                temp_var4 = [1,u[i],u[i]**2, pow(u[i],3)]
                                if j==1:
                                        x_u.append(np.dot(temp_var4,temp_var_coord)[0]) #calculate x coords
                                else:
                                        y_u.append(np.dot(temp_var4,temp_var_coord)[0]) #calculate y coords

                #calculate (x,y) for the lower curve of the airfoil 
                for j in range(2):
                        temp_var =  np.array([[A[j]],[B[j]],[TA_l[j]],[TB_l[j]]]) #control points for x and y coords
                        temp_var_coord = np.dot(temp_var1,temp_var)
                        for i in range(len(u)):
                                temp_var4 = [1,u[i],u[i]**2, pow(u[i],3)]
                                if j==1:
                                        x_l.append(np.dot(temp_var4,temp_var_coord)[0]) #calculate x coords
                                else:
                                        y_l.append(np.dot(temp_var4,temp_var_coord)[0]) #calculate y coords
                coords = y_l,x_l,y_u,x_u
                return coords 

        def save(self,fichier):
                ''' sauvegarde sur fichier des pts du profil au format xfoil'''
                xl,yl,xu,yu = self.spline()
                # points du profil
                N = len(xl)
                X = np.zeros(2*N-1)
                Y = np.zeros(2*N-1)
                X[:N]   = xl[::-1]
                X[N-1:] = xu[:]
                Y[:N]   = yl[::-1]
                Y[N-1:] = yu[:]
                #
                titre="nurbs {} {} {} {} {} {}".\
                        format(self.ta_u,self.ta_l,self.tb_u,self.tb_l,self.alpha_b,self.alpha_c)
                np.savetxt(fichier,np.transpose([X,Y]),comments=' ',header=titre)
                return

        def plot(self):
                ''' draw the profil with the control point '''
                xl,yl,xu,yu = self.spline()
                # trace avec les pts de controle 
                ax = plt.axes()
                plt.xlim(-0.1,1.1)
                plt.ylim(-.2,.2)
                colour = 'r' 
                plt.plot(xl,yl, color = colour) #plot the spline
                ax.arrow(0.,0.,0.,self.ta_u/10.,head_width=0.03,head_length=0.05,fc=colour,ec=colour)
                alpha = np.pi-(self.alpha_b+self.alpha_c)*np.pi/180
                ax.arrow(1.,0.,self.tb_u/10.*np.cos(alpha),self.tb_u/10.*np.sin(alpha),\
                        head_width=0.03,head_length=0.05,fc=colour,ec=colour)
                colour = 'g' 
                plt.plot(xu,yu, color = colour)
                ax.arrow(0.,0,0.,-self.ta_l/10.,head_width=0.03,head_length=0.05,fc=colour,ec=colour)
                alpha = np.pi-(self.alpha_c)*np.pi/180
                ax.arrow(1.,0.,self.tb_u/10.*np.cos(alpha),self.tb_u/10.*np.sin(alpha),\
                        head_width=0.03,head_length=0.05,fc=colour,ec=colour)
                plt.plot([0,1],[0,0],'o',ms=6)
                plt.axis('equal')
                return

# test

def _example(): 
        '''Runs an example'''
        k ={}
        #sample coefficients: Coefficients for generating NACA5410
        k['ta_u'] = 0.1584
        k['ta_l'] = 0.1565
        k['tb_u'] = 2.1241
        k['tb_l'] = 1.8255
        k['alpha_b'] = 11.6983
        k['alpha_c'] = 3.8270

        af = NURBS(k)
        print(af)
        af.save("profil.dat")
        plt.figure(figsize=(12,6))
        af.plot()
        plt.show()

#If this file is run, execute example
if __name__ == "__main__":
        _example()
