#! /usr/bin/env python3
# (C) Marc BUFFAT département mécanique, UCB Lyon 1
# interface WEB pour les outils de validation
import os,sys
import subprocess
import platform
import glob
from pathlib import Path
import pandas as pd
import shutil
import tarfile
import datetime
import requests
from   urllib.parse import quote

from flask import Flask, render_template, flash, Response, session, abort, send_from_directory, request, redirect
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import Navbar,View,Link
from flask_fontawesome import FontAwesome
# liste des cours
def list_cours():
    '''determine la liste des cours sur le serveur'''
    api_url = 'http://127.0.0.1:8081/hub/api/'
    token   =  'X8m5h64J8q',
    # 
    r = requests.get(api_url+'services', headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    # liste des services de cours
    data = r.json()
    listcours = []
    for service in data:
        name = data[service]['name']
        cde  = data[service]['command']
        if 'jupyterhub-singleuser' in cde:
            listcours.append(name)
    return listcours

# barre navigation
topbar = Navbar(
               View('<Jupyter', 'get_jupyter'),
               View('Home',  'get_home'),
               View('Cours', 'get_cours'),
               View('TP',    'get_tp'),
               View('Doc',   'get_doc'),
               View('Nbgrader',   'get_nbgrader'),
               View('FAQ',   'get_faq'),
               View('About', 'get_about'),

              )
# Link('link','url')
nav = Nav()
nav.register_element('top',topbar)
# interface WEB pour la validations de notebook

app = Flask(__name__, instance_relative_config=True)
Bootstrap(app)
app.config.from_pyfile('config.py')
# fonts
fa = FontAwesome(app)
app.config['FONTAWESOME_STYLES'] = ['brands','solid']
# configuration
ENV     = app.config['ENV']
PYTHON  = ENV+"/bin/python3"
my_env = os.environ.copy()
my_env["PATH"] = app.config['PATH'] + my_env["PATH"]
my_env["VIRTUAL_ENV"] = ENV
# config
DEBUG   = app.config['DEBUG']
BASEDIR = app.config['BASEDIR']
if DEBUG: print("config:",ENV,PYTHON,"MYENV=",my_env,"basedir=",BASEDIR)
# FLASK
FLASKPATH = app.config['FLASKPATH']
FLASKURL  = app.config['FLASKURL']
if DEBUG: print("flask:",FLASKPATH,FLASKURL)
# liste des cours
COURS = app.config['COURS']
if DEBUG: print("config:",COURS)
# cours sur le serveur
listcours = list_cours()
LISTCOURS = []
for cours in COURS:
    if cours in listcours: LISTCOURS.append(cours)
if DEBUG: print("cours:",listcours,'->',LISTCOURS)
# determine la liste des equipes pédagogiques et des responsables / cours
ADMIN = {}
RESP  = {}
for cours in LISTCOURS:
    try:
      f = open(BASEDIR+cours+"/hub_config.py",'r')
      users = []
      line = f.readline()
      while line:
          if "name:" in line:
              user = f.readline()
              while "]" not in user:
                  users.append(user.strip().replace("'","").replace(",",""))
                  user = f.readline()
              break
          else:
              line = f.readline()
      f.close()
      if len(users) == 0 :
          print("Erreurs pas d'équipe pédagagogique pour ",cours,users)
          sys.exit(1)
      else:
          ADMIN[cours] = users
          RESP[cours] = users[0:2]
    except OSError as err:
      print("Erreur lecture ",BASEDIR+cours+"hub_config.py")
      sys.exit(1)
if DEBUG:
    print("enseignants:",ADMIN," responsables:",RESP)
# determine la liste des TP / cours
TPLIST = {}
for cours in LISTCOURS:
    TPLIST[cours] =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/*cfg"))]
#
if DEBUG: print("liste cours/TP:",LISTCOURS,ADMIN,TPLIST)
# execute script python
def run_pyscript(script,arg,FLASH=True):
    result="Erreur"
    try:
      result = subprocess.check_output(PYTHON+' '+script+' '+arg , shell=True, text=True, env=my_env )
      if FLASH: flash("python script execution OK","info")
    except subprocess.CalledProcessError as err:
      if DEBUG: print("erreur code",err.returncode,err.output)
      flash("python script erreur code{} {}".format(err.returncode,err.output),"danger")
    return result
# execute une ligne de code python
def run_pyline(line,FLASH=True):
    result="Erreur"
    try:
       result = subprocess.check_output(PYTHON+' -c "' + line + '"' , shell=True, text=True, env=my_env )
       if FLASH: flash("python inline execution OK","info")
    except subprocess.CalledProcessError as err:
       if DEBUG: print("erreur code",err.returncode,err.output)
       flash("python inline erreur code{} {}".format(err.returncode,err.output),"danger")
    return result
# run script bash
def run_bash(script,FLASH=True):
    result="Erreur"
    try:
       bash = 'bash -c "{}"'.format(script)
       result = subprocess.check_output(bash, shell=True, text=True, env=my_env )
       if FLASH: flash("bash script execution OK","info")
    except subprocess.CalledProcessError as err:
       if DEBUG: print("erreur code",err.returncode,err.output)
       flash("bash script erreur code{} {}".format(err.returncode,err.output),"danger")
    return result
#
# executions de scripts TP
# ========================
#
# bilanTP avec autograde
@app.route('/bilanTPautograde')
def bilanTPautograde():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="1 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP -a -l validation/liste_{}.txt -b validation {})".format(COURS,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)  
# version sans autograde
@app.route('/bilanTP')
def bilanTP():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="2 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP -l validation/liste_{}.txt -b validation {})".format(COURS,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)  
# version avec sortie pdf 
@app.route('/bilanTPpdf')
def bilanTPpdf():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in  RESP[COURS] : 
        abort(403, description="3 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP --pdf -a -l validation/liste_{}.txt -b validation {})".format(COURS,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)  
# version avec sortie pandoc pdf
@app.route('/bilanTPpandoc')
def bilanTPpandoc():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="4 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP --pandoc -a -l validation/liste_{}.txt -b validation {})".format(COURS,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)  
# version debug
@app.route('/bilanTPdebug')
def bilanTPdebug():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="5 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP -d -a -l validation/liste_{}.txt -b validation {})".format(COURS,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)  
# etudiant pour un tp
@app.route('/add_etudiants_tp/<TP>')
def add_etudiants_tp(TP):
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="6 {} n est pas responsable du cours".format(session['username']))
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    flist = 'validation/liste.txt'
    ftp = 'validation/liste_{}.txt'.format(TP)
    # optimisation 
    if os.path.exists(COURS+"/"+flist) and os.path.exists(COURS+"/"+ftp) and (os.stat(COURS+"/"+flist).st_size == os.stat(COURS+"/"+ftp).st_size) :
            shutil.copy(COURS+"/"+flist,COURS+"/"+ftp)
            return get_tp()
    script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
    result = run_bash(script)
    shutil.copy(COURS+"/"+flist,COURS+"/"+ftp)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)

# ajoute les étudiants a partir des notebooks
@app.route('/add_notebooks')
def add_notebooks():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="7 {} n est pas responsable du cours".format(session['username']))
    script = '(cd {}; liste_notebooks.sh {} validation/liste_{}.txt; add_students.sh validation/liste_{}.txt)'.format(COURS,TP,TP,TP)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
# recupere les TP
@app.route('/collect')
def collect():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="8 {} n est pas responsable du cours".format(session['username']))
    script='(cd {}; nbgrader collect --update {})'.format(COURS,TP)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
# test de similitude sur tous les groupes de TP
@app.route('/simil_GrpTP')
def simil_GrpTP():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="9 {} n est pas responsable du cours".format(session['username']))
    # liste des etudiants
    flist = 'validation/liste.txt'
    ftp = 'validation/liste_{}.txt'.format(TP)
    result = ""
    # optimisation 
    if os.path.exists(COURS+"/"+flist) and os.path.exists(COURS+"/"+ftp) and (os.stat(COURS+"/"+flist).st_size == os.stat(COURS+"/"+ftp).st_size) :
        shutil.copy(COURS+"/"+flist,COURS+"/"+ftp)
    else:
        script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
        result += run_bash(script)
        shutil.copy(COURS+"/"+flist,COURS+"/"+ftp)
    # liste des cours de TP associés au cours
    COURSTP = ""
    LST = glob.glob(COURS+"G*")
    for nom in LST:
        COURSTP += " " + nom
    if COURSTP == "": abort(404, description="aucun cours de TP associé à {}".format(COURS))
    # test similitude 
    script = "(cd {}; bilanTP -a -l validation/liste_{}.txt -b validation {} --simil {})".format(COURS,TP,TP,COURSTP) 
    result += run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)

# validation auto d'un TP
@app.route('/valide_auto')
def valide_auto():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="9 {} n est pas responsable du cours".format(session['username']))
    # collecte des TP
    script='(cd {}; nbgrader collect --update {})'.format(COURS,TP)
    result = run_bash(script)
    # liste des etudiants
    flist = 'validation/liste.txt'
    ftp = 'validation/liste_{}.txt'.format(TP)
    # optimisation 
    if os.path.exists(COURS+"/"+flist) and os.path.exists(COURS+"/"+ftp) and (os.stat(COURS+"/"+flist).st_size == os.stat(COURS+"/"+ftp).st_size) :
        shutil.copy(COURS+"/"+flist,COURS+"/"+ftp)
    else:
        script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
        result += run_bash(script)
        shutil.copy(COURS+"/"+flist,COURS+"/"+ftp)
    # validation avec autograde
    script = "(cd {}; bilanTP -a -l validation/liste_{}.txt -b validation {})".format(COURS,TP,TP) 
    result += run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)
#
# scripts cours
# =============
#
# configuration auto d'un cours
@app.route('/config_auto')
def config_auto():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="10 {} n est pas responsable du cours".format(session['username']))
    flist = 'validation/liste.txt'
    script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
    result = run_bash(script)
    script = '(cd {}; set_acl.sh {} validation/liste.txt)'.format(COURS,COURS)
    result += run_bash(script)
    # ajout des enseignants
    script = "("
    for prof in RESP[COURS]:
        script += 'setfacl -m u:{}:r-x /srv/nbgrader/exchange/{};'.format(prof,COURS)
    script += ")"
    if DEBUG: print("config:",script)
    result += run_bash(script)
    #
    return render_template('script_long.html', run_script=script, res_script=result, cours=COURS)  
# definit les droits acl
@app.route('/set_acl')
def set_acl():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    script = '(cd {}; set_acl.sh {} validation/liste.txt)'.format(COURS,COURS)
    result = run_bash(script)
    # ajout des enseignants
    script = "("
    for prof in RESP[COURS]:
        script += 'setfacl -m u:{}:r-x /srv/nbgrader/exchange/{};'.format(prof,COURS)
    script += ")"
    if DEBUG: print("set_acl:",script)
    result += run_bash(script)
    #
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
@app.route('/reset_acl')
def reset_acl():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    script = '(setfacl -b /srv/nbgrader/exchange/{}; setfacl -m o:r-x /srv/nbgrader/exchange/{}; getfacl -p -t /srv/nbgrader/exchange/{} )'.format(COURS,COURS,COURS)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
# ajoute les étudiants a partir de la liste des inscrits
@app.route('/add_etudiants')
def add_etudiants():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if session['username'] not in RESP[COURS] : 
        abort(403, description="11 {} n est pas responsable du cours".format(session['username']))
    flist = 'validation/liste.txt'
    script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {} add_students.sh {})'.format(COURS,flist,flist,flist)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
# liste des etudiants
@app.route('/list_users')
def list_users():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    script=FLASKPATH+'bin/list_users.py'
    result = run_pyscript(script,COURS)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
@app.route('/list_db')
def list_db():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    script="(cd {}; nbgrader db student list)".format(COURS)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)
# menus de base
@app.route('/')
def get_home():
    if 'listcours' not in session: abort(403, description="aucun cours autorisé")
    LISTcours = session['listcours']
    return render_template('index.html',listcours=LISTcours)
# cours
@app.route('/cours', methods=['GET'])
def get_cours():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tplist' not in session: abort(404, description="pas de liste de TP selectionnée")
    TPlist = session['tplist']
    # creation repertoire validation et etudiants.txt
    valdir   = BASEDIR + COURS + '/validation'
    if not os.path.exists(valdir): os.makedirs(valdir)
    etufile  = valdir+'/etudiants.txt' 
    if not os.path.exists(etufile): Path(etufile).touch()
    TARlist=[os.path.basename(f) for f in glob.glob(BASEDIR + COURS + "/*tar.gz")]
    return render_template('cours.html',admin=ADMIN[COURS],cours=COURS,TP_list=TPlist,TAR_list=TARlist)
@app.route('/set_cours/<cours>')
def set_cours(cours):
    session['cours']  = cours
    TPLIST[cours] =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/*cfg"))]
    session['tplist'] = TPLIST[cours]
    return get_cours()
# TP
@app.route('/tp', methods=['GET'])
def get_tp():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    return render_template('tp.html',cours=COURS,tp=TP,fichier="bilan_"+TP+".csv",fichier1="notes_"+TP+".csv",
                           fichier2="bilan_similitude_"+TP+".csv")
@app.route('/set_tp/<tp>')
def set_tp(tp):
    session['tp'] = tp
    return get_tp()
# affichage des notes
@app.route('/notes/<file>', methods=['GET'])
def notes(file):
    result=""
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    if 'tp' not in session: abort(404, description="pas de TP selectionné")
    TP = session['tp']
    fichier = COURS + "/validation/"+file
    if os.path.exists(fichier):
        data = None
        if "notes_" in file:
            data = pd.read_csv(fichier,sep="|")
        else:
            data = pd.read_csv(fichier,sep=",")
        data.set_index('id',inplace=True)
        result = data.to_html(classes='table table-striped')
    else:
        flash("le fichier {} n existe pas".format(fichier),"danger")
    return render_template('notes.html',cours=COURS,tp=TP,notes=result)
# exportation tomuss des notes (colonne notes du fichier)
@app.route('/tomuss/<file>', methods=['GET'])
def tomuss(file):
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    fichier = COURS + "/validation/"+file
    ID    = []
    NOTES =[]
    if os.path.exists(fichier): 
        data = None
        if "notes_" in file:
            data = pd.read_csv(fichier,"|")
        else:
            data = pd.read_csv(fichier,sep=",")
        ID    = data['id' ].to_list()
        NOTES = data['note'].to_list()
    else:
        flash("le fichier {} n existe pas".format(fichier),"danger")
    return render_template('tomuss.html',IdNotes=zip(ID,NOTES))
# exportation tomuss des commentaires (colonne comment du fichier)
@app.route('/tomuss_comment/<file>', methods=['GET'])
def tomuss_comment(file):
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    fichier = COURS + "/validation/"+file
    ID      = []
    COMMENT = []
    if os.path.exists(fichier): 
        data = pd.read_csv(fichier,sep="|")
        ID    = data['id' ].to_list()
        COMMENT = data['comment'].to_list()
    else:
        flash("le fichier {} n existe pas".format(fichier),"danger")
    return render_template('tomuss.html',IdNotes=zip(ID,COMMENT))
# creation d'un fichier de notes TP pour notation manuelle
@app.route('/notesTP/<nomtp>', methods=['GET'])
def notesTP(nomtp):
    TP = nomtp
    COURS = session['cours']
    script=FLASKPATH+'bin/gestion_notes.py'
    result = run_pyscript(script,TP+" "+COURS)
    return render_template('script.html', run_script=script, res_script=result, cours=TP)
# about
@app.route('/about', methods=['GET'])
def get_about():
    EXPORT = run_bash('export',FLASH=False)
    PY_ENV = run_pyline("import os; print(os.environ)",FLASH=False)
    PY_PATH= run_pyline("import sys; print(sys.path)",FLASH=False)
    PY_SUB = run_pyline("import sys; print(sys.executable)",FLASH=False)
    PWD    = run_pyline("import os; print(os.getcwd())",FLASH=False)
    return render_template('about.html',ses=session,syspath=sys.path,osenv=os.environ,pyexe=sys.executable,
                            pypath=PY_PATH,export=EXPORT,python3=PY_SUB,pyenv=PY_ENV,pwd=PWD)
# doc
@app.route('/doc', methods=['GET'])
def get_doc():
    #return send_from_directory('static','documentation.html')
    return render_template('documentation.html')
@app.route('/nbgrader', methods=['GET'])
def get_nbgrader():
    return render_template('doc_nbgrader.html')
@app.route('/faq', methods=['GET'])
def get_faq():
    return render_template('faq.html')
# loggin
@app.route('/logging/<user>', methods=['GET'])
def logging(user):
    if DEBUG: print("logging user:",user)
    session['username'] = user
    listcours = []
    for cours in LISTCOURS:
        if user in ADMIN[cours]:
            listcours.append(cours)
    if len(listcours) :
        session['listcours'] = listcours
    else:
        abort(403,description="aucune gestion de cours autorisée")
    return render_template('index.html',listcours=listcours)
# jupyter
@app.route('/jupyter', methods=['GET'])
def get_jupyter():
    host = request.host
    url = 'https://'+host
    #if 'cours'  in session: 
    #    url = 'https://'+host+"/services/"+cours
    #else:
    #    url = 'https://'+host
    if DEBUG: print("url:",url)
    return redirect(url)

# sauvegarde dans un fichier zip
@app.route('/archivage', methods=['GET'])
def archivage():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    x = datetime.datetime.now()
    archive = "{}/validation_{}.tar.gz".format(COURS,x.strftime("%d_%b_%Y"))
    # efface les archives precedentes
    #for f in glob.glob("COURS/validation*.tar.gz"):
    #    os.remove(f)
    with tarfile.open(archive,"w:gz") as f:
        fichier = '{}/gradebook.db'.format(COURS)
        if os.path.isfile(fichier):
            f.add(fichier)
        for root, dirs, files in os.walk('{}/validation'.format(COURS)):
            for fichier in files:
                f.add(os.path.join(root, fichier))
        for root, dirs, files in os.walk('{}/submitted'.format(COURS)):
            for fichier in files:
                f.add(os.path.join(root, fichier))
    f.close()
    script = "(tar -tvf {})".format(archive) 
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)  
# purge cours
@app.route('/purge', methods=['GET'])
def purge():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    COURS = session['cours']
    script = "(sudo {}/bin/purge_cours.sh {})".format(FLASKPATH,COURS) 
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=COURS)  

#
nav.init_app(app)
# display configuration
if DEBUG: print(app.config)
#
if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000)
