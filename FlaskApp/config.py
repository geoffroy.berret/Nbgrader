"""Flask configuration."""
from os import environ, path

TESTING = False
DEBUG = False
ENV = 'production'
SECRET_KEY = 'X8m5h64J8q'
# flask
FLASKPATH= "/home/cours/FlaskApp/"
FLASKURL = "flask_cours"
ENV  = "/var/lib/jupyterhub/venvs/py3"
PATH = "/var/lib/jupyterhub/venvs/py3/bin:/usr/local/Validation:"+FLASKPATH+"/bin:"
# liste des cours geres (lien dans static)
BASEDIR  = "/home/cours/"
COURS = ['PythonL1','MGC2005L','MGC2005LG1','MGC2005LG2','MGC2005LG3','MGC2014L','MGC3062L','MGC1061M','MGC1056M','MGC2367M','MGC2367MG1','MGC2367MG2','PL9014ME','IntroPython','PythonScientifique','MGCtest','DatabaseIA','IntroIA']

