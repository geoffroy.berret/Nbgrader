#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
   echo "syntaxe: $0 cours liste_etudiants"
   exit 1
fi
cours=$1
etudiants=$2
rep="/srv/nbgrader/exchange/$cours"
# test si existe
if [ ! -d $rep ]
then
   mkdir $rep
fi
# reset acl
setfacl -b $rep
# par defaut aucun etudiant
setfacl -m o:--- $rep
# ajoute la liste des etudiants (compte)
if [ ! -s $etudiants ]
then
   echo "aucun étudiant dans le cours ($etudiants)"
else
   while IFS="" read -r p || [ -n "$p" ]
   do
      setfacl -m u:$p:r-x $rep
   done < $etudiants
fi
# affiche les acl
getfacl -p -t $rep

