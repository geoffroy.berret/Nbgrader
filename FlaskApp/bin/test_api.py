#!/usr/bin/env python3
import requests

api_url = 'http://127.0.0.1:8081/hub/api/'
token   =  'X8m5h64J8q',
# using a request token
#api_url = 'https://mbuffat-nbgrader.univ-lyon1.fr/hub/api/'
#token   = '45f5796a987f4c50860555a4c08a082f'
print("test api host={} token={}".format(api_url,token))
# 
r = requests.get(api_url, headers={ 'Authorization': 'token %s' % token, })
r.raise_for_status()
print("version:",r.json())
#
r = requests.get(api_url+'info', headers={ 'Authorization': 'token %s' % token, })
r.raise_for_status()
print("\ninfo:\n",r.json())
# 
r = requests.get(api_url+'groups', headers={ 'Authorization': 'token %s' % token, })
r.raise_for_status()
print("\ngroups:\n",r.json())
# 
r = requests.get(api_url+'users', headers={ 'Authorization': 'token %s' % token, })
r.raise_for_status()
print("\nusers:\n",r.json())
# 
r = requests.get(api_url+'proxy', headers={ 'Authorization': 'token %s' % token, })
r.raise_for_status()
print("\nproxy:\n",r.json())
# 
r = requests.get(api_url+'services', headers={ 'Authorization': 'token %s' % token, })
r.raise_for_status()
print("\nservices:\n",r.json())
# 

