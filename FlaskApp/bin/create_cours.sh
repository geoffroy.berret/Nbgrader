#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
	echo "syntaxe creation du cours: $0 cours"
	exit 1
fi
BASE='/home/cours/NEW_COURSE'
cours=$1
echo "creation du cours $cours"
cp -ar $BASE $cours
cd $cours
sed "s/NEW_COURSE/$cours/" $BASE/hub_config.py > hub_config.py
sed "s/NEW_COURSE/$cours/" $BASE/nbgrader_config.py > nbgrader_config.py

