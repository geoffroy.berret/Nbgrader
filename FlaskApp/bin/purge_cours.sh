#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
	echo "syntaxe purge du cours: $0 cours"
	exit 1
fi
cours=$1
echo "ATTENTION: purge du cours $cours"
cd $cours
rm -rf gradebook.db release/ autograded/ submitted/ validation/bilan* validation/liste*
rm -rf /srv/nbgrader/exchange/$cours/*

