#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
	echo "syntaxe ajout \n a la fin d'un fichier: $0 fichier"
	exit 1
fi
# ajout \n a la fin d'un fichier s'il n'existe pas
sed -i -e '$a\' $1
