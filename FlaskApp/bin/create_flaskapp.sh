#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
	echo "syntaxe creation FlaskApp pour le  meta cours: $0 metacours"
	exit 1
fi
BASE='/home/cours/FlaskApp'
metacours=/home/$1
echo "creation FlaskApp dans meta-cours $metacours"
cd $metacours
mkdir FlaskApp
cd FlaskApp
ln -s $BASE/bin .
ln -s $BASE/static .
ln -s $BASE/templates .
cp -a $BASE/*.py .
cp -a $BASE/*.wsgi .
cp -ar $BASE/instance .
