#! /usr/bin/env bash
#liste les etudiants ayant soumis un TP
if [ "$#" -eq 0 ]; then
	echo "syntaxe $0 nomTP [liste_etu.txt]"
	exit 1
fi
NOMTP=$1
FICHIER="liste_etu.txt"
if [ "$#" -eq 2 ]; then
	FICHIER=$2
fi
echo "creation liste des etudiants pour le TP $NOMTP dans $FICHIER"
find submitted -name $NOMTP | sed s,submitted\/,, | sed s,\/$NOMTP,, > $FICHIER
