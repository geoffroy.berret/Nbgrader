#!/usr/bin/env python3
# creation / mise ajour d'un fichier de notes (manuelles)
import sys,os
import pandas as pd
import configparser as ConfigParser
from shutil import copyfile

if len(sys.argv) != 3 :
    print("ERREUR: aucun nom de TP et de cours spécifies")
    sys.exit(1)

nomTP = sys.argv[1]
fichier = "notes_"+nomTP+".csv"
cours = sys.argv[2]
rep = os.getcwd()+"/"+cours+"/validation/"
cfg = os.getcwd()+"/"+cours+"/"+nomTP+".cfg"
print("creation/mise ajour du fichier de notes {} dans {}".format(fichier,rep))
src = rep+"bilan_"+nomTP+".csv"
dest= rep+fichier
# lecture fichier de configuration
if os.path.isfile(cfg): 
    conf = ConfigParser.RawConfigParser()
    conf.read(cfg)
    # lecture des coefficients
    coef_auto = float(conf.get('NOTE','AUTO'))
    coef_manu = float(conf.get('NOTE','MANU'))
    print("Coefficients note auto={} note manuelle={}".format(coef_auto,coef_manu))
else:
    print("ERREUR: le fichier de configuration {} n'existe pas".format(cfg))
    sys.exit(1)
# lecture du fichier de notes automatiques
if os.path.isfile(src): 
    data = pd.read_csv(src,sep=',')
    ID      = data['id' ]
    ETUDIANT= data['etudiant']
    NOTE    = data['note']
else:
    print("ERREUR: le fichier de notes {} n'existe pas".format(src))
    sys.exit(1)
# creation nouveau dataframe pour notation manuelle
data1 = pd.concat([ID,ETUDIANT,NOTE],axis=1)
data1['comment'] = " commentaire "
data1['auto'] = data1['note'].copy()
data1['manu'] = 0.0
print(data1.info())
if os.path.isfile(dest):
    # lecture des anciennes notes
    print("conservation des anciennes notes dans {}".format(dest))
    data2 = pd.read_csv(dest,sep='|')
    print(data2.info())
    data1['comment'] = data2['comment'].copy()
    data1['manu'] = data2['manu'].copy()
# calcul de la moyenne
    data1['note'] = coef_auto*data1['auto'] + coef_manu*data1['manu']
# sauvegarde du résultat
data1.set_index('id',inplace=True)
data1.to_csv(dest,encoding='utf8',sep='|')
#
