#!/usr/bin/env python3
"""
service validation interface avec  Hub Jupyter
"""

from functools import wraps
import json
import os
from urllib.parse import quote

from flask import Flask, redirect, request, Response, send_from_directory, abort

from jupyterhub.services.auth import HubAuth


prefix = os.environ.get('JUPYTERHUB_SERVICE_PREFIX', '/')

auth = HubAuth(
    api_token=os.environ['JUPYTERHUB_API_TOKEN'],
    cache_max_age=60,
)

app = Flask(__name__)
app.config.from_pyfile('config.py')
DEBUG     = app.config['DEBUG']
FLASKURL  = app.config['FLASKURL']


def authenticated(f):
    """Decorator for authenticating with the Hub"""
    @wraps(f)
    def decorated(*args, **kwargs):
        cookie = request.cookies.get(auth.cookie_name)
        token = request.headers.get(auth.auth_header_name)
        if cookie:
            user = auth.user_for_cookie(cookie)
        elif token:
            user = auth.user_for_token(token)
        else:
            user = None
        #if user and  (not user['admin']): user = None
        resp = f(user, *args, **kwargs)
        #if user: 
        #    resp.set_cookie('userID',user)
        return resp
    return decorated


@app.route(prefix)
@authenticated
def validation(user):
    if (user is None):
        abort(status=403,description="Accès refusé. Vous n'avez pas les droits suffisants !!")
    host = request.host
    url = 'https://'+host+'/'+FLASKURL+'/logging/'+user['name']
    return redirect(url)
    #return redirect('https://mbuffat-nbgrader.univ-lyon1.fr/flask_X8m5h64J8q/')
    #return send_from_directory('static','validation.html')
    #return render_template('validation.html')
