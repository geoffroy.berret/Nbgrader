---
title: FAQ sur les outils de validation
---

# Questions fréquentes

## Configuration

### Quesqu'un meta-cours ?

Un meta cours est un regroupement de cours qui permet une simplification de l'administration et de la gestion des cours.

Par défaut, un nouveau cours est crée sous le meta cours **cours1**. Mais on peut demander la création d'un meta cours pour une formation, qui regroupera
alors tous les cours de cette formation.

### Peut-on partager des notebooks entre plusieurs cours ?

Oui, le système utilise une architecture de fichiers Unix, et donc il est possible de partager des sources de TP en utilisant simplement des liens symboliques.
Il suffit que les cours se trouvent sous le même meta-cours. 
C'est utile pour créer des cours par groupe de TP ou de TD avec les mêmes TP, mais des étudiants différents.

### Comment se fait la gestion des étudiants ?

Par défaut, on doit fournir la liste des numéros des étudiants inscrits au cours dans le fichier **validation/etudiants.txt**, que l'on crée facilement
avec un copier-coller à partir de la table tomuss du cours.

On peut cependant demander à avoir un cours ouvert à tous les étudiants, et dans ce cas c'est l'enseignant qui doit ensuite gérer ces étudiants.

## Jupyter

### Différence entre Jupyter, Jupyter nbgrader, Jupyter nbgrader + validation ?

[**Jupyter**](https://jupyter.org) est un serveur web qui permet de gérer des notebooks ipython (ou R).

[**Jupyter nbgrader**](https://nbgrader.readthedocs.io/en/stable/) est une extension de Jupyter qui permet de gérer des cours. L'enseignant peut donner des notebooks / fichiers aux étudiants, récupérer le travail des étudiants et éventuellement noter le travail des étudiants de façon semi-automatique.

**Jupyter nbgrader + validation** rajoute au serveur Jupyter nbgrader des outils supplémentaires de gestion des cours, et des évaluations à travers une interface web.

### Qu'est ce que Jupyter lab ?

[**Jupyter lab**](https://jupyter.org/)  est une évolution de Jupyter qui en plus des notebooks, offre une interface web de programmation complète avec un éditeur, un terminal et un interpréteur python. Cette interface est mieux adaptée à de la programmation avancée par rapport aux notebooks.

## Enseignant

### à quoi correspond le retour d'execution des commandes ?

On peut l'ignorer tant que la commande c'est bien passée (message en bleu OK en haut de l'écran). 
Il est utile uniquement en cas de problème, pour essayer de le résoudre.

### peut on modifier un notebook déja fournit aux étudiants ?

En général c'est déconseillé. Si on doit absolument le faire, il faut demander aux étudiants de récupérer la dernière version.
Pour cela ils doivent effacer ou renommer le dossier du TP dans le répertoire du cours sous leur compte, puis dans assignements récupérer le TP avec la commande fetch.

### que signifie le bouton **non-fiable** (en haut à droite) ?

par defaut les notebooks que vous téléchargez sont considèrés comme non-fiables, et on interdit l'execution de code java-script et donc certaines fonctionnalités
ne sont pas actives. Je vous conseille donc, si vous savez ce qui est executé dans le notebook, de cliquer sur le bouton pour indiquer que votre notebook est **fiable**.

### que signifie le bouton **validate** en haut ?

il permet de vérifier que le notebook s'execute sans erreur.

## Etudiants

### pourquoi dans assignemnts un étudiant voit toujours ces anciens TP soumis ?

nbgrader maintient un système de cache. Il suffit de purger (effacer) le répertoire **.local/share/jupyter/nbgrader_cache**
dans le home de l'étudiant.

### comment convertir un notebook ipython en rapport au format pdf ?
A la fin du notebook il suffit d'exécuter la commande

        !print_notebook --rm 2 --no-input monTP.ipynb

cette commande est identique à la commande classique

         jupyter nbconvert monTP.ipynb  --to pdf --no-input

avec la suppression des 2 cellules d'entêtes.

On peut aussi  utiliser pandoc plutôt que nbconvert

        !print_notebook --pandoc --rm 2 --no-input monTP.ipynb

**Attention:** la conversion en pdf peut être complexe et poser des problèmes. Il faut donc bien vérifier le résultat. 

## Contact

si vous constatez des erreurs, ou des informations manquantes, n'hésitez pas à contacter l'auteur par mél:
 <a href="https://perso.univ-lyon1.fr/marc.buffat/">Marc BUFFAT (département mécanique, UCB Lyon 1)</a> 

