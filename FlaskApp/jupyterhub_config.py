import os
import sys

c = get_config()

name1 = '__GESTION_cours'
c.JupyterHub.services.append(
    {
        'name': name1,
        'url': 'http://127.0.0.1:10101',
    	'oauth_no_confirm': True,
    	'user': 'cours',
   	'cwd': os.path.dirname(os.path.realpath(__file__)),
        'command': ['flask', 'run', '--port=10101'],
        'environment': {
            'FLASK_APP': 'validation_flask.py',
        }
    })
