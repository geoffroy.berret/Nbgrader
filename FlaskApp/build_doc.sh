#! /usr/bin/env bash
echo "building documentation and FAQ"
if [ "$#" -eq 1 ] && [[ "$1" == '-pdf' ]]; then
echo "generation pdf"
pandoc --metadata=author:"Marc Buffat, dpt mécanique, Lyon 1" -s -t html5 doc_nbgrader.md  -o doc_nbgrader.pdf
#pandoc -s -t html5 documentation.md  -o documentation.pdf
fi
# option -N pour numeroter toc
pandoc -s --toc --toc-depth 3 --template template.html -t html documentation.md  -o templates/documentation.html
pandoc -s --toc --toc-depth 3 --template template.html -t html faq.md  -o templates/faq.html
pandoc -s --toc --toc-depth 3 --template template.html -t html doc_nbgrader.md  -o templates/doc_nbgrader.html
