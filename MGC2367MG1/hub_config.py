# coding: utf-8
import os

c = get_config()

name = 'MGC2367MG1'

c.JupyterHub.load_groups.update({
    name: [
        'marc.buffat', 
        'frederic.alizard',
    ]
})
c.JupyterHub.services.append({
    'name': name,
    'url': 'http://127.0.0.1:9996',
    'oauth_no_confirm': True,
    'user': 'cours',
    'cwd': os.path.dirname(os.path.realpath(__file__)),
    'command': ['jupyterhub-singleuser', f'--group={name}', ],
})

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
