#! /usr/bin/env python3
#
# NOM PRENOM numero étudiant
#
# analyse de l'ecoulement autour d'un naca
import numpy as np
import re
# donnees sur le naca 
class Naca():
    def __init__(self,E,X0,Y0):
        # caracteristique du profil (epaisseur, cambrure)
        self.e  = E
        self.xa = X0
        self.ya = Y0
        # points sur le profil
        self.X=None
        self.Y=None
        # donnees fct de l'angle d'incidence alpha
        self.alpha=None
        self.Pr=None
        self.U =None
        self.V =None
        return
# extraction des valeurs de alpha
def read_alpha(line):
    mots = line.split("@")[1:]
    alpha=[]
    for mot in mots:
        alpha.append(float(re.findall(r'-?\d+\.?\d*',mot.split("=")[1])[0]))
    return  alpha
# lecture des donnees
def lecture(fichier,E,Xa,Ya):
    ''' renvoie  la structure naca epaisseur E, pt cambrure xa,ya '''

    return 
# lecture vitesse
def lecture_vitesse(fichier,naca):
    ''' lecture de la vitesse autour du profil  naca'''

    return 
# portance 
def portance(naca):
    ''' calcul de la portance '''

    return 
# circulation
def circulation(naca):
    ''' calcul de la circulation '''

    return
# validation
def validation():
    import matplotlib.pyplot as plt

    return
#
if __name__ == "__main__":
    validation()
