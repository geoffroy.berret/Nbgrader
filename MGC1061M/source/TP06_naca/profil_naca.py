#! /usr/bin/env python3
from numpy import *
# ecriture format DFX d'une courbe données par les pts X,Y,Z
# si la courbe est fermée, le dernier point = premier point
def writedxf(nom,X,Y,Z):
    fid=open(nom,'w');
    fid.write('999\nCourbe au format DXF\n 0\nSECTION\n 2\nENTITIES\n 0\n')
    for i in range(len(X)-1):
        fid.write('LINE\n 8\n 0\n');
        # ecriture du segment Pi-Pi+1
        fid.write('10\n %.8f\n 20\n %.8f\n 30\n %.8f\n'%(X[i],Y[i],Z[i]))
        fid.write('11\n %.8f\n 21\n %.8f\n 31\n %.8f\n'%(X[i+1],Y[i+1],Z[i+1]))
        fid.write(' 0\n');
    fid.write('ENDSEC\n 0\nEOF\n');
    fid.close();
    return
#
