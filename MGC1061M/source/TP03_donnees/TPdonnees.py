#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# auteur NOM:xxxxx  PRENOM:yyyyy   NUMERO_ETUDIANT:zzzzz

import numpy as np
import matplotlib.pyplot as plt
import glob
from mabib import *

class Data():     
    '''structure de donnees associé à chaque fichier'''     
    def __init__(self,fichier,alpha):
       ''' initialisation'''
       self.nom = fichier
       self.alpha = alpha
       # statistiques (moyenne)
       self.Xm = None
       self.Ym = None 
       # lissage
       self.a = None 
       self.b = None
       return
# liste des fichiers de donnees
print("liste des fichiers de données")
Fichiers=glob.glob("data/*.dat")
for k in range(len(Fichiers)):
   nom = Fichiers[k]    
   print("le fichier {} s'appelle {}",k,nom)
# analyse des donnees

# tracer des résultats (courbe)