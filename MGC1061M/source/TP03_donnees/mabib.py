# -*- coding: utf-8 -*-
# auteur NOM:xxxxx  PRENOM:yyyyy   NUMERO_ETUDIANT:zzzzz
# canvas programme Python
# bibliotheque
import numpy as np
import matplotlib.pyplot as plt
# consigne: 
#  - programmation fonction 
#    bien spécifier les arguments et la valeur de retour
#  - verification
#    a la fin du fichier dans la section :
#         if __name__ == '__main__'
#    on appelle la fonction avec un jeux d'arguments pour vérifier

# implementation
def val_alpha():
    print("\tATTENTION: fonction  non emplémentée")
    return
def val_count():
    print("\tATTENTION: fonction  non emplémentée")
    return
def val_XY():
    print("\tATTENTION: fonction  non emplémentée")
    return
def val_moyenne():
    print("\tATTENTION: fonction  non emplémentée")
    return
def val_lissage():
    print("\tATTENTION: fonction  non emplémentée")
    return

# test de vérification
if __name__ == '__main__':
    print("verification val_alpha")
    val_alpha()
    print("verification val_count")
    val_count()
    print("verification val_XY")
    val_XY()
    print("verification val_moyenne")
    val_moyenne()
    print("verification val_lissage")
    val_lissage()

