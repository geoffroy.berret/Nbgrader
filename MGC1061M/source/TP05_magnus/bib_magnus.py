#! /usr/bin:env python3
# analyse de l'effet magnus
import numpy as np
import re

# donnees sur le cylindre
class Cylindre():
    def __init__(self):
        # points sur le cercle
        self.X=None
        self.Y=None
        # donnees fct de theta1
        self.theta1=None
        self.Pr=None
        return
# extraction des valeurs de theta1 stockées sur une ligne line (format comsol)
def read_theta1(line):
    mots = line.split("@")[1:]
    theta1=[]
    for mot in mots:
        # derniere valeur numérique du mot
        theta1.append(float(re.findall(r'-?\d+\.?\d*',mot.split("=")[1])[0]))
    return theta1
# lecture des donnees
def lecture(fichier):
    ''' renvoie une structure cylindre initialisee'''
    return
# portance & traine
def portance(cyl):
    return 
def trainee(cyl):
    return 
# solution exacte en X,Y pour une valeur x1,y1 du pt d'arret
def Pr_ex(x1,y1,X,Y):
    return

# validation des fonctions de la bibliotheque
def validation():
    import matplotlib.pyplot as plt

    return
#
if __name__ == "__main__":
    validation()
