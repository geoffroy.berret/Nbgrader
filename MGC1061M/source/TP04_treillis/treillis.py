#! /usr/bin/env python3
# bibliotheque pour l'etude d'un treillis
# (C) NOM PRENOM NUMERO_ETUDIANT 
import numpy as np
import matplotlib.pyplot as plt

# definition de la structure treillis
class Treillis():
    """ structure de données pour un treillis plan 2D """
    def __init__(self,mon_fichier):
        self.fichier = mon_fichier
        # coordonnees des neuds
        self.nn = 0
        self.X  = None
        # conditions aux limites (code 0,1,2,3)
        self.CL = None
        # forces nodales
        self.FCL= None
        # numéro des 2 nds de chaque barre
        self.ne = 0
        self.G  = None
        # section, module d'young , masse volumique des barres (constant)       
        self.S  = None
        self.E  = None
        self.rho = None
#
def trace_treillis(tr,titre):
    """ tracer du treillis tr avec le numero des nds"""
    # calcul du deplacement des nds 
    Xd = tr.X.copy()
    # tracer des nds
    plt.plot(Xd[:,0],Xd[:,1],'o',markersize=10,color='b')
    # et des numeros
    for i in range(tr.nn):
            plt.text(Xd[i,0],Xd[i,1],str(i),fontsize=18)
    for i in range(tr.ne):
        n1=tr.G[i,0]
        n2=tr.G[i,1]
        plt.plot([Xd[n1,0],Xd[n2,0]],[Xd[n1,1],Xd[n2,1]],'-g',lw=3)
    plt.axis('equal')
    plt.title("%s Nb=%d Nn=%d "%(titre,tr.ne,tr.nn))
    plt.draw()
    return
#
def lecture(nom_fichier):
    """ lecture des donnees d'un treillis dans le fichier mon_fichier"""
    # creation d'une structure treillis vide
    tr = Treillis(nom_fichier)
    # lecture du fichier

    # initialisation des données dans la structure treillis tr

    # renvoie la structure tr
    return tr
#
def long_barres(tr):
    """ calcul vecteur numpy contenant la longueur des barres du treillis tr"""

    return
#
def rotation_barre(tr,l):
    """ pour un treillis tr, calcule la matrice de rotation (4x4) pour la barre l (de 0 a ne-1)"""

    return
#
def matrice_rigidite(tr):
    """ pour un treillis tr, calcule par assemblage la matrice de rigidite avant application des cdts aux limites"""

    return
#
def conditions_limites(tr,AA,BB):
    """ pour un treillis tr, une matrice AA et un second membre BB, applique les CL sur la matrice AA et le second membre BB, et renvoie la matrice A et le second membre B après application des CL"""

    return
#
def matrice_masse(tr):
    """pour un treillis tr, calcule par assemblage la matrice de masse avant application des cdts aux limites"""

    return
#
# validation
#
if __name__ == "__main__":
    #
    fichier = 'mon_treillis.dat'
    # creation du treillis
    tr=lecture(fichier)
    # affiche le treillis
    print("Treillis {} nbre nds={} nbre de barres={}".format(tr.fichier,tr.nn,tr.ne))
    # a completer

