# -*- coding: utf-8 -*-
# auteur NOM:xxxxx  PRENOM:yyyyy   NUMERO_ETUDIANT:zzzzz
# canvas programme Python
# bibliotheque
import numpy as np
import matplotlib.pyplot as plt
# consigne: 
#  - programmation fonction 
#    bien spécifier les arguments et la valeur de retour
#  - verification
#    a la fin du fichier dans la section :
#         if __name__ == '__main__'
#    on appelle la fonction avec un jeux d'arguments pour vérifier

# implementation
def ma_fonction():
    print("\tATTENTION: fonction  non emplémentée")
    return

# test de vérification
if __name__ == '__main__':
    print("verification ma_fonction")
    ma_fonction()

