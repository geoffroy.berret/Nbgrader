# -*- coding: utf-8 -*-
# auteur NOM:xxxxx  PRENOM:yyyyy   NUMERO_ETUDIANT:zzzzz
# canvas programme Python
# bibliotheque
import numpy as np
import matplotlib.pyplot as plt
# consigne: 
#  - programmation exercise
#   pour l'exercise i on écrit la fonction funci() en spécifiant les arguments
#  - verification
#    a la fin du fichier dans la section :
#         if __name__ == '__main__'
#    on appelle la fonction avec un jeux d'arguments pour vérifier
#  - validation
#    on utilise la fonction valide_exo pour passer les tests de validation:
#        (uniquement si aucune erreur de syntaxe)

# implementation
def func0():
    print("\tATTENTION: fonction func0 non emplémentée")
    return
def func1():
    print("\tATTENTION: fonction func1 non emplémentée")
    return
def func2():
    print("\tATTENTION: fonction func2 non emplémentée")
    return
def func3():
    print("\tATTENTION: fonction func3 non emplémentée")
    return
def func4():
    print("\tATTENTION: fonction func4 non emplémentée")
    return
def func5():
    print("\tATTENTION: fonction func5 non emplémentée")
    return
def func6():
    print("\tATTENTION: fonction func6 non emplémentée")
    return
def func7():
    print("\tATTENTION: fonction func7 non emplémentée")
    return
def func8():
    print("\tATTENTION: fonction func8 non emplémentée")
    return
def func9():
    print("\tATTENTION: fonction func9 non emplémentée")
    return
def func10():
    print("\tATTENTION: fonction func10 non emplémentée")
    return


# test de vérification
if __name__ == '__main__':
    print("verification func0")
    func0()
    print("verification func1")
    func1()
    print("verification func2")
    func2()
    print("verification func3")
    func3()
    print("verification func4")
    func4()
    print("verification func5")
    func5()
    print("verification func6")
    func6()
    print("verification func7")
    func7()
    print("verification func8")
    func8()
    print("verification func9")
    func9()
    print("verification func10")
    func10()

