exercices du cours M1 Atelier numérique

# attention purge *py et *dat
find . -name "*py"
find . -name "*dat"
# automatic grading
nbgrader autograde --create TP02b_planete
nbgrader autograde --create --force --no-execute TP02b_planete
nbgrader autograde --create --force --student=p1405765 --no-execute TP02b_planete
#
nbgrader autograde  --force --Autograde.exclude_overwriting="{'TP02b_planete':['CRplanete.tex','planete.py','planetes.dat','TPplanete.py','CRplanete.pdf']}" TP02b_planete
# bilan notes (avec note max 20)
../bin/bilan_notes.py -M 20 TP02b_planete notes1.csv
# feedback
nbgrader feedback TP02b_planete
sudo python3 ~/bin/return_feedback.py TP02B_planete
# bilanTP
bilanTP TP02b_planete

# test de validation dans un notebook
%load_ext autoreload
from validation.validation import check_function
%autoreload 2
from mabib import val_alpha
assert check_function(val_alpha,'exo210')
