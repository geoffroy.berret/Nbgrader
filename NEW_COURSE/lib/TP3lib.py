# -*- coding: utf8 -*-
# librairie python pour les cours dans lib
# (C) M. BUFFAT 
'''
 fonction python pour TP3 
'''
import sys,os
from   random import randint, random
import numpy as np
import matplotlib.pyplot as plt
#
# test calcul somme_carres
#
def test_somme_carres(pge):
    '''
calcul de la somme des carrés des n premiers entiers
    '''

    for k in range(5):
        n = randint(1,100)
        sol = n*(n+1)*(2*n+1)//6
        S = pge(n)
        if S!=sol:
            print("validation: ",test_somme_carres.__name__,
                                 test_somme_carres.__doc__)
            print("Resultat FAUX avec arg:",n,"=> res:",S)
            return False
        return True

# trace d'une liste de valeurs
def trace_vals(X,Y,titre=None):
    plt.rc('font', family='serif', size='18')
    plt.figure(figsize=(12,6))
    plt.plot(X,Y,linestyle='dashed',marker='o',markersize=12)
    plt.xlabel('x')
    plt.ylabel('y')
    if titre is not None:
        plt.title(titre)
    plt.grid(True);
    return
