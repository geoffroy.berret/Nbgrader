# -*- coding: utf8 -*-
# librairie python pour les cours dans lib
# (C) M. BUFFAT 
'''
 fonction python pour TP1 
'''
import sys,os
from   random import randint, random
import numpy as np
import matplotlib.pyplot as plt

import string
def rand_str(n,upcase=False):
    """génére une chaine aléatoire de longueur n"""
    if upcase :
        ALPHABET = np.array(list(string.ascii_uppercase))
    else:
        ALPHABET = np.array(list(string.ascii_letters + ' '))
    chaine = ''
    for i in range(n):
        chaine += np.random.choice(ALPHABET)
    return chaine

def exo001(pge,opt):
    """renvoie le nombre de caractères 
         dans une chaine égale à l'un des 2 caractères"""
    chaine = rand_str(100)
    n = chaine.count(opt[0]) + chaine.count(opt[1])
    res = pge(chaine)
    if n != res:
        print("validation: ",exo001.__name__,exo001.__doc__)
        print("Resultat FAUX : ",res)
        return False
    return True

def exo002(pge,opt):
    """programme qui prend comme argument une chaine de catactères majuscules
       et qui renvoie son codage en utilisant la clé de 2 caractères"""
    chaine = rand_str(30,upcase=True)
    # genere codage
    k1 = ord(opt[0])-ord(chaine[0])
    k2 = ord(opt[2])-ord(chaine[1])
    secret = ""
    for i in range(len(chaine)):
        # decalage
        if i%2 == 0 :
            k = k1
        else:
            k = k2
        # calcul du code du caractère
        ich = ord(chaine[i])+k
        # test si   
        if ich > ord('Z'):
            ich = ich - ord('Z') + ord('A')
        elif ich < ord('A'):
            ich = ich + ord('Z') - ord('A')
        # codage
        secret += chr(ich)
    # test
    res = pge(chaine)
    if secret != res:
        print("validation: ",exo002.__name__,exo002.__doc__)
        print("Resultat FAUX : ",res)
        return False
    return True