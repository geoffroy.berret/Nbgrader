
## Prototype de cours d'initiation à Jupyter/Python

Ce cours est un assemblage de divers notebooks utilisés dans mes enseignements en Mécanique utilisant Python 
et l'environnement Jupyter-nbgrader (L2/L3/M1/M2) ainsi qu'un exemple d'un problème simple de balistique  de niveau terminale/L1

Une fois connecté sur le portail (en acceptant le certificat du site qui n'est pas officiel pour l'instant)

- [https://l1-nbgrader.univ-lyon1.fr](https://l1-nbgrader.univ-lyon1.fr/)

vous sélectionnez le menu Assignements et vous avez une liste d'exemple de notebooks de cours et TP
que vous pouvez exécuter en tant qu'étudiants en suivant les consignes. 

1. Introduction:  notebook d'introduction à Python/Jupyter utilisé pour créer un cours en vidéo.

2. Base_de_Python:  base de programmation en Python, utilisé de façon interactive dans un cours de niveau L2

3. TP_chaine_liste: TP d'applications du cours précédent 

4. Numpy-Matplotlib: utilisation des tableaux numpy et tracé de courbe avec matplotlib, utilisé de façon interactive dans un cours de niveau L2

5. TP_traitement_donnees: exemple de traitement de données sur le réchauffement climatique, version simplifié du TP de L2

6. Balistique: notebook jupyter classique sur la balistique qui m'a servit à créer 2 notebooks de TP

7. TP_Balistique: mise sous forme de TP (différents pour chaque étudiant) sous nbgrader (avec tests automatiques  de validation et notation des réponses des étudiants) dans une version analyse scientifique et modélisation numérique d'un problème de balistique.

8. TP_Angry_Birds: version ludique sous forme de jeux de ce problème de balistique

ces exemples sont un aperçu de ce qu'il ai possible de faire. Pour des exemples dans de nombreuses autres disciplines, vous pouvez allez sur le site jupyter.org

- [https://nbviewer.jupyter.org/](https://nbviewer.jupyter.org/)

ou celui de Laurena Barba

-[Teaching and Learning with Jupyter](https://jupyter4edu.github.io/jupyter-edu-book/)
