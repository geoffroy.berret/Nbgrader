#!/bin/sh
# Print already used port un hub_config.py

find $PWD -maxdepth 2 -type f -name hub_config.py -exec grep -PHo "(?<=127.0.0.1:)\d+" '{}' \;

# vim: fileencoding=utf8
