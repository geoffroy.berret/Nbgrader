#! /usr/bin/env python3
"""Return feedback to students that was generated with `nbgrader feedback`.

This must be run from the root of the nbgrader directory. You probably need to run
it with sudo since you need to write to other users' directories.

Usage:

    sudo python return_feedback problem_set_name

dans le repertoire du cours 
    
"""

import os
import shutil
import pwd,grp
import argparse
import stat
try:
  import configparser as ConfigParser
except:
  import ConfigParser

# lecture configuration
def read_config(nom):
    conf = ConfigParser.RawConfigParser()
    conf.read(nom)
    monIPYNB = conf.get('IPYNB','NOMIPYNB')
    return monIPYNB
#
def set_permissions(path, uid, gid):
    os.chown(path, uid, gid)
    if os.path.isdir(path):
        os.chmod(path, stat.S_IRUSR | stat.S_IRGRP | stat.S_IXUSR | stat.S_IXGRP)
    else:
        os.chmod(path, stat.S_IRUSR | stat.S_IRGRP)


def main(name, student=None, force=False):
    feedback_dir = os.path.abspath("feedback")
    cours = os.path.basename(os.getcwd())
    ipynb = read_config(name + ".cfg")

    print("feedback {} cours {} ipynb {}".format(feedback_dir,cours,ipynb))
    if student is None:
        # grab student names that are already in the feedback directory
        students = sorted(os.listdir(feedback_dir))
    else:
        students = [student]

    for student in students:
      # get student info
      p = pwd.getpwnam(student)
      group = grp.getgrgid(p[3])[0]
      gecos = p[4].split(' ')
      print(student,group,gecos[0],gecos[1],gecos[1]+'.'+gecos[0]+'@etu.univ-lyon1.fr')
      # update source directory
      src = os.path.join(feedback_dir, student, name)
      dst = os.path.join("/home/{}/{}/{}/feedback/{}".format(group,student,cours,name))
      tp  = os.path.join("/home/{}/{}/{}/{}".format(group,student,cours,name))
      # check if exist
      if not os.path.exists(src):
          print("no feedback {} ".format(src))
      else : 
        # remove existing feedback if it exists
        if os.path.exists(dst):
            if force:
                print("removing '{}'".format(dst))
                shutil.rmtree(dst)
            else:
                print("skipping {}, feedback already exists".format(student))
                continue

        # copy the feedback
        print("'copy {}' --> '{}'".format(src, dst))
        shutil.copytree(src, dst)
        # link
        srcf = dst+"/"+ipynb.replace(".ipynb",".html")
        dstf = tp +"/feedback.html"
        print("link '{}' --> '{}'".format(srcf,dstf))
        if os.path.exists(dstf):
            os.remove(dstf)
        os.symlink(srcf,dstf)
        # get the uid and gid
        pwinfo = pwd.getpwnam(student)
        uid = pwinfo.pw_uid
        gid = pwinfo.pw_gid

        # set the owner to be the student and permissions to be read-only
        set_permissions(dst, uid, gid)
        for dirname, dirnames, filenames in os.walk(dst):
            for f in (dirnames + filenames):
                path = os.path.join(dirname, f)
                set_permissions(path, uid, gid)
    #
    return

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('name', help='the name of the assignment')
    parser.add_argument('--student', default=None, help='the name of a specific student')
    parser.add_argument('--force', action="store_true", default=False,
                        help='overwrite existing feedback (use with extreme caution!!)')
    args = parser.parse_args()
    main(args.name, student=args.student, force=args.force)
