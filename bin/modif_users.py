#! /usr/bin/env python3

import os,sys,pwd,grp

cours = os.getcwd()
print("Modification/mise a jour des étudiants du cours {}".format(cours))
logins=os.listdir(cours+"/submitted/")
print("liste des étudiants:",logins)
for u in logins:
        p = pwd.getpwnam(u)
        g = grp.getgrgid(p[3])[0]
        gecos=p[4].split(' ')
        nom=gecos[0]
        prenom=gecos[1].split(',')[0]
        mail=prenom+"."+nom
        if g == "etu":
            mail=mail+"@etu.univ-lyon1.fr"
        else:
            mail=mail+"@univ-lyon1.fr"
        print("pid={}\t grp={}\t prenom={}\t nom={}\t mail={}".format(u,g,prenom,nom,mail))
        cde = 'nbgrader db student add {} --first-name="{}" --last-name="{}" --email="{}"'.format(u,prenom,nom,mail)
        os.system(cde)
print("Bilan")
os.system("nbgrader db student list")
