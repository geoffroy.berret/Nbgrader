#! /usr/bin/env bash
for dir in */
do
	echo "cleaning $dir"
	cd $dir
	rm -rf release submitted autograded gradebook.db
	ls
	cd ..
done
