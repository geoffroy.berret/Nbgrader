#! /usr/bin/env python3
import nbformat as nbf
import os,sys
#
fichier=sys.argv[1]
try:
    nb = nbf.read(fichier,nbf.NO_CONVERT)

    print("remove cell 'HIDDEN TEST' dans ",fichier)
    for cell in nb['cells']:
        if "BEGIN HIDDEN TEST" in cell['source']:
            print(cell,cell['source'],cell['metadata'])
            cell['metadata']={}
            nb['cells'].remove(cell)
        #print('source:',len(cell['source']))
        elif len(cell['source'])==0 :
            print(cell,cell['source'],cell['metadata'])
            cell['metadata']={}
            nb['cells'].remove(cell)
    nbf.write(nb,fichier)
except:
    print("Erreur remove HIDDEN TEST")
    raise
#
