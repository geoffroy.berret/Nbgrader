#! /usr/bin/env bash
FILE=$1
if [[ $# -ne 1 ]]; then
	echo "syntaxe $0 notebook.ipynb"
	exit 1
fi
# conversion markdown
echo "conversion markdown -> "${FILE}
jupyter nbconvert --to markdown ${FILE}
# extraction des reponses
BASEFILE=`basename ${FILE} .ipynb`
MDFILE="${BASEFILE}.md"
TXTFILE="${BASEFILE}.txt"
echo "extraction des reponses texte -> "${TXTFILE}
sed -n '/=== BEGIN ANSWER ===/,/=== END ANSWER ===/p' ${MDFILE} | sed '/=== BEGIN ANSWER ===/d' | sed '/=== END ANSWER ===/d'  > ${TXTFILE}
# conversion pdf
pandoc ${TXTFILE} -f markdown -t latex -V fontsize=12pt -o ${BASEFILE}.pdf
