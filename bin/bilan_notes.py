#! /usr/bin/env python3
import os,sys
from os import path
import pandas as pd

DEBUG= False
MOYEN= False
MAX  = None
#
if len(sys.argv) == 1:
    print("syntaxe: bilan_note [-d] [-m] [-M xx] assignement_name [fichier.csv]")
    print("-m: calcul moyenne en 1ere colonne")
    print("-M xxx: definit la note max pour le calcul (defaut derniere colonne)")
    print("rem: ajoute une colonne si fichier.csv existe")
    sys.exit(1)
argnum = 1
if sys.argv[argnum] == "-d":
    DEBUG  = True
    argnum = argnum + 1
if sys.argv[argnum] == '-m':
    MOYEN  = True
    argnum = argnum + 1
if sys.argv[argnum] == '-M':
    MAX  = int(sys.argv[argnum+1])
    argnum = argnum + 2
if DEBUG:
    print("DEBUG:{} MOYEN={} MAX={}".format(DEBUG,MOYEN,MAX))
# creation des notes 
fichier = 'notes.csv'
os.system('nbgrader export --to={}'.format(fichier))
# lectures des notes
df = pd.read_csv(fichier)
if DEBUG:
    print("Contenu du fichier {}".format(fichier))
    df.info()
print("liste des assignments : {}".format(df.assignment.unique()))
# selection
assignment=sys.argv[argnum]
argnum = argnum + 1
print("Selection assignment={}".format(assignment))
df1=df.loc[df['assignment'] == assignment]
df2=df1[['student_id','last_name','first_name','max_score','score']].copy()
for idx,row in df2.iterrows():
    student_id = row['student_id']
    if student_id[0] == 'p' :
        student_id=student_id.replace('p','1')
        df2.loc[idx,'student_id'] = student_id
    else:
        df2.drop(idx,inplace=True)
df2['student_id']=df2['student_id'].astype(int)
df2['score']=df2['score'].astype(int)
df2['max_score']=df2['max_score'].astype(int)
# calcul note / s20
if MAX is not None: df2['max_score']=MAX
df2['score']=round(20.0*df2['score']/df2['max_score'])
df2['max_score']=df2['score'].copy()
df2.set_index('student_id',inplace=True)
print("Bilan")
print(df2)
# mise sur fichier
fichier=sys.argv[argnum]
argnum = argnum + 1
if path.exists(fichier):
    df3 = pd.read_csv(fichier,index_col=0,header=None)
    nc = len(df3.columns)
    print("colonnes {}".format(nc))
    #names=['student_id','last_name','first_name','max_score','note1','note2']
    df3[nc+1]=df2['score'].copy()
    # calcul moyenne
    if MOYEN:
        cols=list(range(4,nc+2))
        df3[3] = round(df3[cols].mean(axis=1))
    df3.to_csv(fichier,encoding='utf-8',header=False)
    print("mise ajour {}".format(fichier))
    print(df3.info())
else:
    df2.to_csv(fichier,encoding='utf-8',header=False)
    print("creation {}".format(fichier))
    print(df2.info())
# fin

