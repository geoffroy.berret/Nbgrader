#! /usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

_uid_ = 1234

def dataset_init(n):
    '''init generateur aleatoire pour la BD'''
    _uid_ = n
    np.random.seed(_uid_)
    return

def dataset1(N):
    '''création dataset 1 loi affine'''
    # data X = [x1,x2]
    X = np.zeros((N**2,2))
    y = np.zeros(N**2)
    x1 = np.linspace(1,2,N)
    x2 = np.linspace(1,4,N)
    for i in range(N):
        for j in range(N):
            X[j+i*N] = [x1[i],x2[j]]
    # valeur 
    np.random.seed(_uid_)
    y = np.dot(X, np.array([1, 2])) + 3 + 2*np.random.rand(X.shape[0])
    return X,y

def plot_data1(X,y,titre=""):
    plt.figure(figsize=(10,6))
    plt.scatter(X[:,0],X[:,1],c=y,cmap='jet')
    plt.colorbar()
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$")
    plt.title(titre);
    return

def plot1(N,X,y,Xpred,ypred,titre=""):
    '''tracer du dataset1 en fct de x2'''
    plt.figure(figsize=(10,8))
    plt.plot(Xpred[:,1],ypred,'--r')
    plt.scatter(X[0:N,1],y[0:N],s=100)
    plt.title(titre)
    plt.xlabel("x2");
    return

def dataset2(N):
    X = np.zeros((N**2,2))
    y = np.zeros(N**2)
    # calcul couleur fct de la valeur
    col = ['r']*N**2
    np.random.seed(_uid_)
    for i in range(N**2):
        x = 2*np.random.rand(2)-1
        y[i] = np.sign(x[0])
        if y[i] >=0 : col[i] = 'b'
        X[i] = [x[1]+x[0], x[1]-x[0]]
    return X,y,col

def predict2(NN,predict):
    x = np.linspace(-1,1,NN)
    Xpred = np.zeros((NN**2,2))
    for i in range(NN):
        for j in range(NN):
            Xpred[i+j*NN] = [x[i],x[j]]
    ypred = predict(Xpred)
    # calcul couleur fct de la valeur
    colpred = ['r']*NN**2
    for i in range(NN**2):
        if ypred[i]>=0: colpred[i]='b'
    return Xpred, ypred, colpred

def plot2(X,col,Xpred,colpred,titre=""):
    # tracer du dataset
    plt.figure(figsize=(12,6))
    plt.subplot(1,2,1)
    plt.title("valeur -1=red +1=blue");
    plt.scatter(X[:,0],X[:,1],c=col)
    plt.xlabel("$x_1$"); plt.ylabel("$x_2$");
    plt.subplot(1,2,2)
    plt.title(titre);
    plt.scatter(Xpred[:,0],Xpred[:,1],c=colpred)
    plt.xlabel("$x_1$"); 
    return

def dataset3(N):
    X = np.zeros((N**2,2))
    y = np.zeros(N**2)
    # calcul couleur fct de la valeur
    col = ['r']*N**2
    np.random.seed(_uid_)
    for i in range(N**2):
        x = 2*np.random.rand(2)-1
        y[i] = np.sign(x[0])
        if y[i] >=0 : col[i] = 'b'
        if x @ x < 0.25:
            y[i] =0
            col[i] = 'g'
        X[i] = [x[1]+x[0], x[1]-x[0]]
    return X,y,col

def predict3(NN,predict):
    x = np.linspace(-1,1,NN)
    Xpred = np.zeros((NN**2,2))
    for i in range(NN):
        for j in range(NN):
            Xpred[i+j*NN] = [x[i],x[j]]
    ypred = predict(Xpred)
    # calcul couleur fct de la valeur
    colpred = ['r']*NN**2
    for i in range(NN**2):
        if ypred[i]>0: 
            colpred[i] = 'b'
        elif ypred[i] == 0:
            colpred[i] = 'g'
    return Xpred,ypred,colpred

def plot3(X,col,Xpred,colpred,titre=""):
    # tracer du dataset et de la prediction
    plt.figure(figsize=(12,6))
    plt.subplot(1,2,1)
    plt.title("valeur -1=red +1=blue");
    plt.scatter(X[:,0],X[:,1],c=col)
    plt.xlabel("$x_1$"); plt.ylabel("$x_2$");
    plt.subplot(1,2,2)
    plt.title(titre);
    plt.scatter(Xpred[:,0],Xpred[:,1],c=colpred)
    plt.xlabel("$x_1$");
    return

def serie_temp(ans):
    # data / jours pour 360 Jours /an sur ans annees
    N = 360*ans
    np.random.seed(_uid_)
    # time series
    Ts = np.array([x for x in np.arange(N)])
    ys = [ 1.0*np.sin(2*np.pi*x/360) + 0.5*np.cos(2*np.pi*x/15) + 0.8*x/360 for x in range(N)] + \
          np.random.normal(size=N,scale=0.2)
    return Ts,ys

def dataset4(Ts,ys,n,N,t0):
    # choix d'une fenetre de n jours précédents pour prédir la valeur du jour
    # n taille de la fenetre
    # N nbre de fenetres
    # t0 date de debut prediction
    # 
    t1 = t0 - N - n + 1
    print(f"apprentissage sur une fenetre de {n} jours entre le jour {t1} et {t0}")
    # 
    X  = np.zeros((N,n))
    y  = np.zeros(N)
    t  = np.zeros(N)
    # construction de la base de données
    for i in range(N):
        X[i,:] = ys[t1+i:t1+i+n]
        y[i]   = ys[t1+i+n]
        t[i]   = Ts[t1+i+n]
    return X,y,t

def plot_data4(n,N,t0):
    plt.figure(figsize=(12,6))
    plt.subplot(1,2,1)
    plt.plot(t,y)
    plt.xlabel("jour")
    plt.ylabel("y")
    plt.title("data apprentissage")
    plt.subplot(1,2,2)
    t1 = t0 - N - n + 1
    # 1ere fenetre
    plt.plot(t[0:n]-n,X[0,:])
    plt.plot(Ts[t1:t1+n+1],ys[t1:t1+n+1],'--')
    plt.scatter([t[0]],[y[0]],c='r')
    # derniere
    plt.plot(t[-n:]-1,X[-1,:])
    plt.plot(Ts[t0-n:t0+1],ys[t0-n:t0+1],'--')
    plt.scatter([t[-1]],[y[-1]],c='r')
    plt.xlabel("jour")
    plt.title("first/last window");
    return

def plot4(titre=""):
    plt.figure(figsize=(10,6))
    plt.plot(Ts[t0:t0+n1],ypred)
    plt.plot(Ts[t0-2*n1:t0+1],ys[t0-2*n1:t0+1])
    plt.xlabel("jour")
    plt.title(f"{titre} sur {n1} jours à partir du jour {t0}");
    return
