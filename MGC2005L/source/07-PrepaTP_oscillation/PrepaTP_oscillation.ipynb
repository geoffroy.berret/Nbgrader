{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-b53871fe6b59d2b0",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "# Préparation TP EDO: oscillation d'un système masse ressort\n",
    "**Marc BUFFAT, dpt mécanique, Université Lyon 1 et [1]**\n",
    "\n",
    "[1] inspiré par le cours \"[Engineering Computations](https://github.com/engineersCode)\" du Pr L. Barba (Washington Univ.)\n",
    "\n",
    "<img src=\"./images/spring-mass.png\" style=\"width: 600px;\"/> \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-5e23e0a95f2b0bee",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "Jusqu'à présent,  vous avez appris à:\n",
    "\n",
    "* calculer la vitesse et l'accélération d'un corps, à partir de positions connues au fil du temps, c'est-à-dire utiliser des dérivées numériques;\n",
    "* trouver la description du mouvement (position en fonction du temps) à partir des données d'accélération, en progressant dans le temps avec la méthode d'Euler;\n",
    "* former le vecteur d'état et la forme vectorisée d'un système dynamique du second ordre;\n",
    "* améliorer le modèle simple de chute libre en ajoutant une résistance à l'air.\n",
    "\n",
    "Vous avez également appris que la méthode d'Euler est une méthode de premier ordre: un développement en série de Taylor montre que la méthode d'Euler a une erreur, appelée erreur de troncature,  proportionnelle à l'incrément de temps, $\\Delta t$.\n",
    "\n",
    "Dans cette leçon, nous travaillerons avec des systèmes oscillants. La méthode d'Euler ne fonctionne pas très bien avec les systèmes oscillants, mais nous allons vous montrer un moyen intelligent de résoudre ce problème.\n",
    "\n",
    "Nous réutiliserons également la fonction `iterationEuler ()`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-20599fdd8dbc86fc",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "## Système masse ressort\n",
    "\n",
    "Le système mécanique considéré est une masse $ m $ attachée à un ressort, dans le cas le plus simple sans frottement. La constante élastique du ressort, $ k $, détermine la force de rappel qu'il applique à la masse lorsqu'elle est déplacée d'une distance $ x $. Le système oscille alors d'avant en arrière autour de sa position d'équilibre\n",
    "\n",
    "\n",
    "<img src=\"./images/spring-mass.png\" style=\"width: 400px;\"/> \n",
    "\n",
    "### Système simple  masse ressort sans frottement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-590a4b1cf237b89f",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "La loi de Newton appliqué au système masse ressort sans frottement s'écrit:\n",
    "\n",
    "\\begin{equation}\n",
    "-k x = m \\ddot{x}\n",
    "\\end{equation}\n",
    "\n",
    "En introduisant le paramètre $\\omega = \\sqrt{k/m}$, l'équation du mouvement s'écrit:\n",
    "\n",
    "\\begin{equation}\n",
    "\\ddot{x} + \\omega^2 x = 0\n",
    "\\end{equation}\n",
    "\n",
    "C'est une équation différentielle du second ordre pour la position $x$, dont on connaît la solution analytique qui représente un mouvement harmonique simple:\n",
    "\n",
    "$$x(t) = x_0 \\cos(\\omega t)$$\n",
    "\n",
    "Cette solution represente une oscillation de periode $T = 2 \\pi/ \\omega $  et d'amplitude $x_0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-cb1b1c6e6149ad8f",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "### Mise sous forme vectorielle\n",
    "\n",
    "Il est utile d'écrire une équation différentielle du second ordre comme un ensemble de deux équations du premier ordre: dans ce cas, respectivement pour la position et la vitesse:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "\\dot{x} &=& v \\nonumber\\\\\n",
    "\\dot{v} &=& -\\omega^2 x\n",
    "\\end{eqnarray}\n",
    "\n",
    "Comme nous l'avons fait précédemment, nous écrivons l'état du système comme un vecteur bidimensionnel,\n",
    "\n",
    "\\begin{equation}\n",
    "\\mathbf{x} = \\begin{bmatrix}\n",
    "x \\\\ v\n",
    "\\end{bmatrix},\n",
    "\\end{equation}\n",
    "\n",
    "et l'équation differentielle  sous forme vectorielle:\n",
    "\n",
    "\\begin{equation}\n",
    "\\dot{\\mathbf{x}} = \\mathbf{F}(\\mathbf{x}) \\mbox{ avec } \\mathbf{F}(\\mathbf{x}) = \n",
    "\\begin{bmatrix}\n",
    "v \\\\ -\\omega^2 x\n",
    "\\end{bmatrix}.\n",
    "\\end{equation}\n",
    "\n",
    "Plusieurs avantages viennent de l'écriture de l'équation différentielle sous forme vectorielle, à la fois théorique et pratique. Dans l'étude des systèmes dynamiques, par exemple, le vecteur d'état est dans un espace d'états appelé l'espace des phases, et beaucoup de choses peuvent être apprises en étudiant graphiquement les solutions d'équations différentielles dans l'espace des phases.\n",
    "\n",
    "En pratique, l'écriture de l'équation sous forme vectorielle aboutit à un code plus général et compact. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-ce29b035af7210ee",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "### Etude de la convergence pour Euler\n",
    "\n",
    "Comment calculer l'ordre de convergence d'une méthode numérique? \n",
    "\n",
    "Dans notre cas avec une solution analytique, nous pouvons  calculer directement l'erreur. Tout ce que nous avons à faire est de résoudre numériquement le problème avec différentes valeurs de $\\Delta t$\n",
    "\n",
    "Dans la cellule de code ci-dessous, ecrire le code python pour calculer la valeur de l'erreur dans le tableau `err_euler`  en fonction des valeurs du pas en temps calculées dans le tableau `dt_values` à partir des valeurs de N dans le tableau `N_values`. On choissit comme temps final T une période.\n",
    "\n",
    "**algorithme**\n",
    "\n",
    "1. création des tableaux pas en temps `dt_values` et de l'erreur `err_euler`\n",
    "2. boucle  sur les pas en temps dt contenus dans `dt_values`  (utiliser `enumerate`)\n",
    "    1. calculer la solution numerique `sol_num` avec ce pas en temps dt sur le temps T   \n",
    "    2. calculer la solution analytique aux mêmes temps\n",
    "    3. en déduire l'erreur comme différence entre la solution numérique et analytique à l'instant t=T\n",
    "    4. mettre le résultat dans le tableau `err_euler`\n",
    "7. fin boucle\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-cf0f4fa21b9b6fdd",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "\n",
    "## Methode  de Runge Kutta\n",
    "\n",
    "On a vue que la méthode d'Euler est peu précise pour simuler les systèmes oscillatoires. Regardez à nouveau l'approximation faite par la méthode d'Euler pour obtenir la nouvelle position:\n",
    "\n",
    "\\begin{equation}\n",
    " x(t_i+\\Delta t)   \\approx  x(t_i) + v(t_i) \\Delta t\n",
    "\\end{equation}\n",
    "\n",
    "Euler utilise la valeur de la vitesse au début de l'intervalle de temps pour avancer la solution.\n",
    "\n",
    "Une explication graphique peut aider ici. Rappelez-vous que la dérivée d'une fonction correspond à la pente de la tangente en un point. La méthode d'Euler se rapproche de la dérivée en utilisant la pente au point initial dans un intervalle et avance la position numérique avec cette vitesse initiale. L'esquisse ci-dessous illustre deux étapes d'Euler consécutives sur une fonction à forte courbure.\n",
    "\n",
    "\n",
    "<img src=\"./images/two-euler-steps.png\" style=\"width: 500px;\"/> \n",
    "\n",
    "L'idée pour améliorer la précision est d'utiliser la dérivée au milieu de l'intervalle. Le problème n'est pas trivial car la  dérivée au milieu de l'intervalle dépend de la valeur de l'état au milieu de l'intervalle, que l'on ne connait pas !\n",
    "\n",
    "La méthode de Runge Kutta suivante va permettre justement d'estimer cette dérivée.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-5d7b578f165c10c6",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "### Méthode de Runge Kutta 2\n",
    "L'idée de cette méthode est d'avancer la solution numérique au milieu d'un intervalle de temps avec la méthode d'Euler, en y calculant les dérivées, puis en revenant en arrière et en mettant à jour l'état du système en utilisant les dérivées du point médian. C'est ce qu'on appelle une méthode de Runge Kutta 2.\n",
    "\n",
    "Si nous écrivons la forme vectorielle de l'équation différentielle comme:\n",
    "\\begin{equation}\n",
    "\\dot{\\mathbf{x}} = \\mathbf{F}(\\mathbf{x}),\n",
    "\\end{equation}\n",
    "\n",
    "alors la méthode numérique de Runge Kutta 2 s'écrit:\n",
    "\\begin{align}\n",
    "\\mathbf{x}_{n+1/2}   & = \\mathbf{x}_n + \\frac{\\Delta t}{2} f(\\mathbf{x}_n) \\\\\n",
    "\\mathbf{x}_{n+1} & = \\mathbf{x}_n + \\Delta t \\,\\, \\mathbf{F}(\\mathbf{x}_{n+1/2}).\n",
    "\\end{align}\n",
    "\n",
    "Nous pouvons maintenant écrire une fonction Python pour mettre à jour l'état en utilisant cette méthode. Nous l'appelerons `iterationRK2()`.\n",
    "\n",
    "Ecrire le code Python de cette méthode dans la cellule suivante en s'inspirant du code `iterationEuler()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-f2f7770729a0a87d",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "## Comparaison des méthodes: convergence\n",
    "\n",
    "Nous avons dit que la méthode d'Euler est précise au premier ordre: l'erreur est proportionnelle de $\\Delta t $ à la puissance 1. Nous l'avons montré en utilisant un développement en série de Taylor. Confirmons-le maintenant numériquement et déterminons la précision de la méthode de Runge Kutta 2.\n",
    "\n",
    "Puisque le mouvement harmoniquea une solution analytique simple, nous pouvons directement calculer une mesure de l'erreur de la solution numérique pour un $\\Delta t$ choisie. L'erreur $e(\\Delta t)$ sur l'intervalle de $t_0$ à $T = N \\Delta t$ peut alors etre calculé  comme suit:\n",
    "\n",
    "\\begin{equation}\n",
    "e(\\Delta t)= | X_{N} (T) - Xe_{N}| \n",
    "\\end{equation}\n",
    "\n",
    "où $X_N$ représente la solution numérique à l'instant final T et $Xe_N$ la solution analytique au meme temps.\n",
    "\n",
    "Nous allons donc faire varier $\\Delta t$ et calculer cette erreur en fonction de de $\\Delta t$.\n",
    "\n",
    "**Attention** on compare l'erreur au même temps final T, donc si on change $\\Delta t$, alors le nombre de pas en temps N change de façon a toujours vérifier $T = N \\Delta t$ \n",
    "\n",
    "Pour éviter les problèmes d'arrondis, on se fixera plutôt les valeurs de N dans un tableau `N_values` d'où l'on déduit les pas en temps $\\Delta t = T/N$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-260ead6b35e66808",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "## CR de la préparation\n",
    "\n",
    "Définir dans la cellule ci-dessous les différentes étapes pour faire cette prédiction:\n",
    "\n",
    " - calculs à faire\n",
    " - fonctions python à écrire\n",
    " - validation et test de la mthode\n",
    " - comparaison des méthodes Euler et Runge Kutta\n",
    " - analyse à faire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-b8e51b559b4360ae",
     "locked": false,
     "points": 0,
     "schema_version": 3,
     "solution": true,
     "task": false
    }
   },
   "source": [
    "Ecrire ici votre analyse\n",
    "\n",
    "%% BEGIN SOLUTION\n",
    "\n",
    "%% END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-269cb88982555eab",
     "locked": true,
     "schema_version": 3,
     "solution": false,
     "task": false
    }
   },
   "source": [
    "## References\n",
    "\n",
    "\n",
    "1. Linge S., Langtangen H.P. (2016) Solving Ordinary Differential Equations. In: Programming for Computations - Python. Texts in Computational Science and Engineering, vol 15. Springer, Cham, https://doi.org/10.1007/978-3-319-32428-9_4, open access and reusable under [CC-BY-NC](http://creativecommons.org/licenses/by-nc/4.0/) license.\n",
    "\n",
    "2. Cromer, A. (1981). Stable solutions using the Euler approximation. _American Journal of Physics_, 49(5), 455-459. https://doi.org/10.1119/1.12478\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# FIN"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Contenu",
   "title_sidebar": "Contenu",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "383.991px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
