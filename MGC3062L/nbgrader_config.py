import os
c = get_config()

###############################################################################
# Begin additions by nbgrader quickstart
###############################################################################
c.CourseDirectory.root = os.path.dirname(os.path.realpath(__file__))
c.IncludeHeaderFooter.header = 'header.ipynb'
# Update this list with other assignments you want
c.CourseDirectory.course_id = "MGC3062L"
