#! /usr/bin/env python
import numpy as np
class Fonctionnelle():
    def __init__(self,uid):
        np.random.seed(uid)
        self.N = np.random.randint(40,50)
        self.B = 2*np.random.rand(self.N)-1
        self.A = np.diag(np.random.randint(4,self.N//4,self.N)) +\
                 -2*np.diag(np.ones(self.N-1),-1) +\
                 -2*np.diag(np.ones(self.N-1),+1)
        p = np.random.permutation(self.N)
        self.A[:,:] = self.A[p,:]
        self.A[:,:] = self.A[:,p]
        # solution Xe
        self.Xe = np.linalg.solve(self.A,self.B)
        return
    def dim(self):
        return self.N
    def info(self):
        print("Fonctionnelle J(X) ")
        print("dimension de X :",self.dim())
        print("minimum   de J :",self.val(self.Xe))
        print("pour ||X|| < 1.0")
        return
    def val(self,X):
        """valeur fonctionnelle en X"""
        return  0.5*np.dot(X,self.A @ X) - np.dot(X,self.B)
    def grad(self,X):
        return self.A @ X - self.B 
    def __call__(self,X):
        return self.val(X)
    def err(self,X):
        """erreur / minimum"""
        return np.linalg.norm(X-self.Xe)
    def err1D(self,alpha,D):
        """erreur / minimum 1D dans la direction D"""
        alpha_min = np.dot(D, self.B) / np.dot(D, self.A @ D )
        return np.abs(alpha - alpha_min)
    def min(self):
        return J.val(self.Xe)
