---
title:  "Cours L2 MGC2005L Outils Informatique pour la mécanique"

author: Marc Buffat
institute: Dpt Mécanique, Université Lyon 1
date: Fevrier 2021
aspectratio: 169
fontsize: 12pt
fontfamily: noto-sans
header-includes:
 - \titlegraphic{\includegraphics[height=2cm]{images/dptmeca.png}\hspace*{4.75cm}\includegraphics[height=2cm]{images/logo_lyon1col.jpg}}

---

# Organisation cours L2 MGC2005L

Liste des séances (4*3h cours + 6*3h TP)

- videos préliminaires IntroJupyter et IntroPython
- cours CM interactif avec nbgrader 

 1. séance cours 3h: Chaine_et_liste
 2. séance TP 3h application liste
 3. séance cours 3h: Numpy-Matplotlib
 4. séance TP 3h TP_regression_linéaire
 5. séance TP libre service pour terminer
 6. séance cours 3h: EDO chute libre
 7. séance TP 3h TP_oscillation
 8. séance TP 3h TP_vibration
 9. seance cours 3h: pendule de Foucault calcul formel, linéarisation, EDO
 10. séance TP 3h pendule double linéarisé
 11. séance TP 3h pendule double nonlinaire / chaos


# Liens HTML

 - serveurs Jupyter du département mécanique

   * serveurs Jupyter pour les  TP (nbgrader): 

       - [https://jupyterL2.mecanique.univ-lyon1.fr](https://jupyterL2.mecanique.univ-lyon1.fr)
 
   * portail pédégogique  

       - [https://moodle.mecanique.univ-lyon1.fr](https://moodle.mecanique.univ-lyon1.fr)
