---
title:  "Serveur Jupyter pour les cours en mécanique"

author: Marc Buffat
institute: Dpt Mécanique, Université Lyon 1
date: Novembre 2020
aspectratio: 169
fontsize: 12pt
fontfamily: noto-sans
header-includes:
 - \titlegraphic{\includegraphics[height=2cm]{images/dptmeca.png}\hspace*{4.75cm}\includegraphics[height=2cm]{images/logo_lyon1col.jpg}}

---

# Utilisation du serveur Jupyter / Nbgrader

## Liens HTML

 - serveurs Jupyter du département mécanique

   * serveurs pour les  cours (nbgrader): 

       - [https://jupyterL2.mecanique.univ-lyon1.fr](https://jupyterL2.mecanique.univ-lyon1.fr)
       - [https://jupyterL3.mecanique.univ-lyon1.fr](https://jupyterL3.mecanique.univ-lyon1.fr)
       - [https://jupyterM1.mecanique.univ-lyon1.fr](https://jupyterM1.mecanique.univ-lyon1.fr)
       - [https://jupyterM2.mecanique.univ-lyon1.fr](https://jupyterM2.mecanique.univ-lyon1.fr)

   * serveur Lyon1: 

       - [https://jupyter.mecanique.univ-lyon1.fr](https://jupyter.mecanique.univ-lyon1.fr)
