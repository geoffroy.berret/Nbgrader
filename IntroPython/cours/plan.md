---
title:  "UE-MGC2005L: Outils Informatique pour la mécanique"
subtitle: "Python/Jupyter pour les scientifiques"

author: Marc Buffat
institute: Dpt Mécanique, Université Lyon 1
date:  2021
aspectratio: 169
fontsize: 12pt
fontfamily: noto-sans
header-includes:
 - \titlegraphic{\includegraphics[height=2cm]{images/dptmeca.png}\hspace*{4.75cm}\includegraphics[height=2cm]{images/logo_lyon1col.jpg}}

---

# Module d'introduction à Python & Jupyter
## Objectifs
 - aucune expérience de programmation préalable
 - utilisation un environnement numérique Python avec des notebooks jupyter
 - $\leadsto$  résolution de problème ou analyse de données scientifiques.

## Liens HTML

 - contenu de la séance  sur  le site 

   * [https://perso.univ-lyon1.fr/marc.buffat](https://perso.univ-lyon1.fr/marc.buffat/COURS/INTROPYTHON_HTML/Introduction.html) 

 - serveur Jupyter du département mécanique

   * Lyon1: [https://jupyter.mecanique.univ-lyon1.fr](https://jupyter.mecanique.univ-lyon1.fr)
   * cours L2: [https://jupyterL2.mecanique.univ-lyon1.fr](https://jupyterL2.mecanique.univ-lyon1.fr)

## NOTES
