{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TP oscillation d'un système masse ressort\n",
    "**Marc BUFFAT, dpt mécanique, Université Lyon 1 et [1]**\n",
    "\n",
    "[1] inspiré par le cours \"[Engineering Computations](https://github.com/engineersCode)\" du Pr L. Barba (Washington Univ.)\n",
    "\n",
    "<img src=\"./images/spring-mass.png\" style=\"width: 600px;\"/> "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Jusqu'à présent,  vous avez appris à:\n",
    "\n",
    "* calculer la vitesse et l'accélération d'un corps, à partir de positions connues au fil du temps, c'est-à-dire utiliser des dérivées numériques;\n",
    "* trouver la description du mouvement (position en fonction du temps) à partir des données d'accélération, en progressant dans le temps avec la méthode d'Euler;\n",
    "* former le vecteur d'état et la forme vectorisée d'un système dynamique du second ordre;\n",
    "* améliorer le modèle simple de chute libre en ajoutant une résistance à l'air.\n",
    "\n",
    "Vous avez également appris que la méthode d'Euler est une méthode de premier ordre: un développement en série de Taylor montre que la méthode d'Euler a une erreur, appelée erreur de troncature,  proportionnelle à l'incrément de temps, $\\Delta t$.\n",
    "\n",
    "Dans cette leçon, nous travaillerons avec des systèmes oscillants. La méthode d'Euler ne fonctionne pas très bien avec les systèmes oscillants, mais nous allons vous montrer un moyen intelligent de résoudre ce problème.\n",
    "\n",
    "Nous réutiliserons également la fonction `eulerstep ()`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "plt.rc('font', family='serif', size='14')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def eulerstep(state, rhs, dt):\n",
    "    '''Update a state to the next time increment using Euler's method.\n",
    "    \n",
    "    Arguments\n",
    "    ---------\n",
    "    state : array of dependent variables\n",
    "    rhs   : function that computes the RHS of the DiffEq\n",
    "    dt    : float, time increment\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    next_state : array, updated after one time increment'''\n",
    "    \n",
    "    next_state = state + rhs(state) * dt\n",
    "    return next_state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Système masse ressort\n",
    "\n",
    "Le système mécanique considéré est une masse $ m $ attachée à un ressort, dans le cas le plus simple sans frottement. La constante élastique du ressort, $ k $, détermine la force de rappel qu'il applique à la masse lorsqu'elle est déplacée d'une distance $ x $. Le système oscille alors d'avant en arrière autour de sa position d'équilibre\n",
    "\n",
    "\n",
    "<img src=\"./images/spring-mass.png\" style=\"width: 400px;\"/> \n",
    "### Système simple  masse ressort sans frottement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La loi de Newton appliqué au système masse ressort sans frottement s'écrit:\n",
    "\n",
    "\\begin{equation}\n",
    "-k x = m \\ddot{x}\n",
    "\\end{equation}\n",
    "\n",
    "En introduisant le paramètre $\\omega = \\sqrt{k/m}$, l'équation du mouvement s'écrit:\n",
    "\n",
    "\\begin{equation}\n",
    "\\ddot{x} + \\omega^2 x = 0\n",
    "\\end{equation}\n",
    "\n",
    "C'est une équation différentielle du second ordre pour la position $x$, dont on connaît la solution analytique qui représente un mouvement harmonique simple:\n",
    "\n",
    "$$x(t) = x_0 \\cos(\\omega t)$$\n",
    "\n",
    "Cette solution represente une oscillation de periode $T = 2 \\pi/ \\omega $  et d'amplitude $x_0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mise sous forme vectorielle\n",
    "\n",
    "Il est utile d'écrire une équation différentielle du second ordre comme un ensemble de deux équations du premier ordre: dans ce cas, respectivement pour la position et la vitesse:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "\\dot{x} &=& v \\nonumber\\\\\n",
    "\\dot{v} &=& -\\omega^2 x\n",
    "\\end{eqnarray}\n",
    "\n",
    "Comme nous l'avons fait précédemment, nous écrivons l'état du système comme un vecteur bidimensionnel,\n",
    "\n",
    "\\begin{equation}\n",
    "\\mathbf{x} = \\begin{bmatrix}\n",
    "x \\\\ v\n",
    "\\end{bmatrix},\n",
    "\\end{equation}\n",
    "\n",
    "et l'équation differentielle  sous forme vectorielle:\n",
    "\n",
    "\\begin{equation}\n",
    "\\dot{\\mathbf{x}} = \\begin{bmatrix}\n",
    "v \\\\ -\\omega^2 x\n",
    "\\end{bmatrix}.\n",
    "\\end{equation}\n",
    "\n",
    "Plusieurs avantages viennent de l'écriture de l'équation différentielle sous forme vectorielle, à la fois théorique et pratique. Dans l'étude des systèmes dynamiques, par exemple, le vecteur d'état est dans un espace d'états appelé l'espace des phases, et beaucoup de choses peuvent être apprises en étudiant graphiquement les solutions d'équations différentielles dans l'espace des phases.\n",
    "\n",
    "En pratique, l'écriture de l'équation sous forme vectorielle aboutit à un code plus général et compact. Écrivons une fonction pour obtenir le second membre  de l'équation différentielle masse-ressort, sous forme vectorielle.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def springmass(state):\n",
    "    '''Computes the right-hand side of the spring-mass differential \n",
    "    equation, without friction.\n",
    "    \n",
    "    Arguments\n",
    "    ---------   \n",
    "    state : array of two dependent variables [x v]^T\n",
    "    \n",
    "    Returns \n",
    "    -------\n",
    "    derivs: array of two derivatives [v - ω*ω*x]^T\n",
    "    '''\n",
    "    \n",
    "    derivs = np.array([state[1], -omega**2*state[0]])\n",
    "    return derivs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous définissons les paramètres du système, choisissons un intervalle de temps égal à 1-20e de la période d'oscillation, et décidons de résoudre le mouvement pour une durée égale à 3 périodes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# parametres\n",
    "omega = 2\n",
    "period = 2*np.pi/omega\n",
    "dt = period/20  # we choose 20 time intervals per period \n",
    "T = 3*period    # solve for 3 periods\n",
    "N = round(T/dt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ensuite, on initialise le tableau des temps et les conditions initiales, et on initialise le tableau des solutions avec des valeurs nulles et on affecte les valeurs initiales aux premiers éléments du tableau des solutions.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = np.linspace(0, T, N)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#initialize solution array\n",
    "x0 = 2    # initial position\n",
    "v0 = 0    # initial velocity\n",
    "num_sol = np.zeros([N,2])\n",
    "#Set intial conditions\n",
    "num_sol[0,0] = x0\n",
    "num_sol[0,1] = v0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous sommes maintenant prêts à résoudre le problème! En parcourant les incréments de temps, en appele la fonction `eulerstep ()` avec le membre de droite de `springmass` et l'incrément de temps comme entrées."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(N-1):\n",
    "    num_sol[i+1] = eulerstep(num_sol[i], springmass, dt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Maintenant, calculons la position en fonction du temps en utilisant la solution analytique connue, afin de la comparer avec le résultat numérique. Puis  nous traçons sur un graphique à la fois des valeurs numériques et analytiques.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_an = x0*np.cos(omega * t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot solution with Euler's method\n",
    "fig = plt.figure(figsize=(10,5))\n",
    "\n",
    "plt.plot(t, num_sol[:, 0], linewidth=2, linestyle='--', label='Euler')\n",
    "plt.plot(t, x_an, linewidth=1, linestyle='-', label='Analytique')\n",
    "plt.xlabel('Temps [s]')\n",
    "plt.ylabel('$x$ [m]')\n",
    "plt.title(\"Système masse ressort avec la méthode d'Euler.\\n\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La solution numérique présente une croissance marquée en amplitude au cours du temps, ce qui n'est certainement pas ce que l'on observe sur le système physique. Quel est le problème avec la méthode d'Euler?\n",
    "\n",
    "Deux questions se posent:\n",
    "\n",
    " 1. Est ce lié à la méthode d'approximation (erreur numérique) ?\n",
    " 2. Est ce lié au modèle mathématique (erreur d'approximation) ?\n",
    " \n",
    " Pour répondre, nous allons tout d'abord refaire l'expérience avec un pas en temps beaucoup plus petit. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### exercise: \n",
    "\n",
    "* Essayez de répéter le calcul ci-dessus en utilisant des valeurs plus petites de l'incrément de temps, `dt`, et regarder si les résultats s'améliorent. Essayez avec `dt = T/40`, `T/160` et `T/2000`.\n",
    "\n",
    "* Bien que le dernier cas, avec 2000 pas par oscillation, semble assez bon, regarder ce qui se passe si vous augmentez ensuite le temps de simulation, par exemple à 20 périodes. \n",
    "\n",
    "* Exécutez à nouveau la simulation.\n",
    "* Que voyez-vous maintenant?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# votre calcul\n",
    "dt = period/2000  # we choose 20 time intervals per period \n",
    "T = 3*period    # solve for 3 periods\n",
    "N = round(T/dt)\n",
    "t = np.linspace(0, T, N)\n",
    "# solution numerique\n",
    "num_sol = np.zeros((N,2))\n",
    "num_sol[0,0] = x0\n",
    "num_sol[0,1] = v0\n",
    "for i in range(N-1):\n",
    "    num_sol[i+1] = eulerstep(num_sol[i], springmass, dt)\n",
    "# solution analytique\n",
    "x_an = x0*np.cos(omega * t)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot solution with Euler's method\n",
    "fig = plt.figure(figsize=(10,5))\n",
    "\n",
    "plt.plot(t, num_sol[:, 0], linewidth=2, linestyle='--', label='Euler')\n",
    "plt.plot(t, x_an, linewidth=1, linestyle='-', label='Analytique')\n",
    "plt.xlabel('Temps [s]')\n",
    "plt.ylabel('$x$ [m]')\n",
    "plt.title(\"Système masse ressort avec la méthode d'Euler.\\n\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### analyse\n",
    "On observe systématiquement une croissance d'amplitude dans la solution numérique, qui s'aggrave avec le temps. La solution s'améliore lorsque nous réduisons l'incrément de temps «dt» (comme il se doit), mais l'amplitude affiche toujours une croissance non physique pour des simulations plus longues."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Amélioration de la méthode d'Euler\n",
    "\n",
    "Le fait est que la méthode d'Euler pose un problème fondamental avec les systèmes oscillatoires. Regardez à nouveau l'approximation faite par la méthode d'Euler pour obtenir la nouvelle position\n",
    "\n",
    "\\begin{equation}\n",
    " x(t_i+\\Delta t)   \\approx  x(t_i) + v(t_i) \\Delta t\n",
    "\\end{equation}\n",
    "\n",
    "Il utilise la valeur de la vitesse au début de l'intervalle de temps pour avancer la solution.\n",
    "\n",
    "Une explication graphique peut aider ici. Rappelez-vous que la dérivée d'une fonction correspond à la pente de la tangente en un point. La méthode d'Euler se rapproche de la dérivée en utilisant la pente au point initial dans un intervalle et avance la position numérique avec cette vitesse initiale. L'esquisse ci-dessous illustre deux étapes d'Euler consécutives sur une fonction à forte courbure.\n",
    "\n",
    "\n",
    "<img src=\"./images/two-euler-steps.png\" style=\"width: 500px;\"/> \n",
    "\n",
    "### Analyse des deux étapes d'Euler sur une fonction à forte courbure\n",
    "\n",
    "Du fait que la méthode d'Euler fait une approximation linéaire pour caluler la solution suivante, en utilisant la valeur de la dérivée au début de l'intervalle, ce n'est pas très bon sur les fonctions oscillatoires.\n",
    "\n",
    "Une idée pour améliorer la méthode d'Euler est alors d'utiliser la valeur mise à jour des dérivées pour la seconde équation.\n",
    "\n",
    "La méthode d'Euler de base s'écrit:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "x(t_0) = x_0, \\qquad x_{i+1} &=& x_i + v_i \\Delta t \\nonumber\\\\\n",
    "v(t_0) = v_0, \\qquad v_{i+1} &=& v_i - {\\omega}^2 x_i \\Delta t\n",
    "\\end{eqnarray}\n",
    "\n",
    "Et si dans la seconde équation sur $v$ nous utilisons la valeur $x_ {i + 1}$ qui vient d'être calculée.\n",
    "\n",
    "\\begin{eqnarray}\n",
    "x(t_0) = x_0, \\qquad x_{i+1} &=& x_i + v_i \\Delta t \\nonumber\\\\\n",
    "v(t_0) = v_0, \\qquad v_{i+1} &=& v_i - {\\omega}^2 x_{i+1} \\Delta t\n",
    "\\end{eqnarray}\n",
    "\n",
    "Remarquez le $ x_ {i + 1} $ dans le second membre de la deuxième équation: c'est la valeur mise à jour, donnant l'accélération à la fin de l'intervalle de temps. Ce schéma modifié est appelé méthode **d'Euler semi-implicite** ou **Euler-Cromer**, (A. Cromer l'a étudié en 1981 [2] pour résoudre des problèmes de mécanique).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Étudiez attentivement la fonction ci-dessous - cela aide beaucoup si vous écrivez des choses sur une feuille de papier!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def euler_cromer(state, rhs, dt):\n",
    "    '''Update a state to the next time increment using Euler-Cromer's method.\n",
    "    \n",
    "    Arguments\n",
    "    ---------\n",
    "    state : array of dependent variables\n",
    "    rhs   : function that computes the RHS of the DiffEq\n",
    "    dt    : float, time increment\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    next_state : array, updated after one time increment'''\n",
    "    \n",
    "    mid_state = state + rhs(state)*dt # Euler step\n",
    "    mid_derivs = rhs(mid_state)       # updated derivatives\n",
    "    \n",
    "    next_state = np.array([mid_state[0], state[1] + mid_derivs[1]*dt])\n",
    "    \n",
    "    return next_state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Définition des paramètres "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = 2\n",
    "period = 2*np.pi/omega\n",
    "dt = period/200  # time intervals per period \n",
    "T = 800*period   # simulation time, in number of periods\n",
    "N = round(T/dt)\n",
    "\n",
    "print('The number of time steps is {}.'.format( N ))\n",
    "print('The time increment is {}'.format( dt ))\n",
    "\n",
    "# time array\n",
    "t = np.linspace(0, T, N)\n",
    "\n",
    "x0 = 2    # initial position\n",
    "v0 = 0    # initial velocity\n",
    "\n",
    "#initialize solution array\n",
    "num_sol = np.zeros([N,2])\n",
    "\n",
    "#Set intial conditions\n",
    "num_sol[0,0] = x0\n",
    "num_sol[0,1] = v0\n",
    "\n",
    "for i in range(N-1):\n",
    "    num_sol[i+1] = euler_cromer(num_sol[i], springmass, dt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recalculez la solution analytique et tracez-la à côté de la solution numérique, lorsque vous êtes prêt. Nous avons calculé un nombre fou d'oscillations, nous devrons donc choisir soigneusement la plage de temps à tracer.\n",
    "\n",
    "Tout d'abord, obtenez la solution analytique. Nous avons choisi de tracer ensuite les premières périodes du mouvement oscillatoire: numérique et analytique.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x_an = x0*np.cos(omega * t) # analytical solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iend = 800 # in number of time steps\n",
    "\n",
    "fig = plt.figure(figsize=(10,8))\n",
    "\n",
    "plt.plot(t[:iend], num_sol[:iend, 0], linewidth=2, linestyle='--', label='Euler-Cromer')\n",
    "plt.plot(t[:iend], x_an[:iend], linewidth=1, linestyle='-', label='Analytique')\n",
    "plt.xlabel('Time [s]')\n",
    "plt.ylabel('$x$ [m]')\n",
    "plt.title('Système masse ressort avec  Euler-Cromer.\\n')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La courbe montre que la méthode 'Euler-Cromer n'a pas le problème de croissance de l'amplitude, ce qui est assez satisfaits.\n",
    "\n",
    "Mais si nous traçons la solution à la fin d'une longue période de simulation, vous pouvez voir qu'elle commence à s'écarter de la solution analytique\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "istart = 400\n",
    "\n",
    "fig = plt.figure(figsize=(10,8))\n",
    "\n",
    "plt.plot(t[-istart:], num_sol[-istart:, 0], linewidth=2, linestyle='--', label='Numerical solution')\n",
    "plt.plot(t[-istart:], x_an[-istart:], linewidth=1, linestyle='-', label='Analytical solution')\n",
    "plt.xlabel('Time [s]')\n",
    "plt.ylabel('$x$ [m]')\n",
    "plt.title('Spring-mass system, with Euler-Cromer method. \\n');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'examen des dernières oscillations sur une très longue période montre une légère différence de phase, même avec un très petit incrément de temps. Ainsi, bien que la méthode Euler-Cromer corrige un gros problème avec la méthode d'Euler, elle comporte toujours une erreur. C'est toujours une méthode de premier ordre!\n",
    "\n",
    "**La méthode Euler-Cromer est une méthode d'Euler semi-implicite qui  est précise du premier ordre, tout comme la méthode d'Euler. L'erreur globale est proportionnelle à $\\Delta t $.**\n",
    "\n",
    "\n",
    "**Note**\n",
    "\n",
    "Vous trouverez souvent la présentation de la méthode Euler-Cromer avec l'ordre inverse des équations, c'est-à-dire l'équation de vitesse résolue en premier, puis l'équation de position résolue avec la valeur mise à jour de la vitesse. Cela ne fait aucune différence dans les résultats: ce n'est qu'une convention entre physiciens.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Convergence\n",
    "\n",
    "Nous avons dit que la méthode d'Euler et la variante de Cromer sont précises au premier ordre: l'erreur est proportionnelle de $\\Delta t $ à la puissance 1. Nous l'avons montré en utilisant un développement en série de Taylor. Confirmons-le maintenant numériquement.\n",
    "\n",
    "Puisque le mouvement harmonique simple a une solution analytique simple, nous pouvons directement calculer une mesure de l'erreur faite avec solution numérique. L'erreur sur l'intervalle de $t_0$ à $T = N / \\Delta t$ peut etre calculé  comme suit:\n",
    "\n",
    "\\begin{equation}\n",
    "e = x_N - x_0 \\cos(\\omega T)\n",
    "\\end{equation}\n",
    "\n",
    "où $x_N$ représente la solution numérique au $N$ième pas de temps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comment confirmer l'ordre de convergence d'une méthode numérique? Dans le scénario chanceux d'avoir une solution analytique pour calculer directement l'erreur, tout ce que nous avons à faire est de résoudre numériquement avec différentes valeurs de $\\Delta t$\n",
    "\n",
    "et voyez si l'erreur varie vraiment linéairement avec ce paramètre.\n",
    "\n",
    "Dans la cellule de code ci-dessous, nous calculons la solution numérique avec différents incréments de temps. Nous utilisons deux instructions for imbriquées: l'une itère sur les valeurs de $\\Delta t$\n",
    ", et l'autre itère sur les pas de temps entre la condition initiale et l'instant final. Nous sauvegardons les résultats dans une nouvelle variable appelée `num_sol_time` qui est un tableau de tableaux. Vérifiez-le!\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt_values = np.array([period/50, period/100, period/200, period/400])\n",
    "T = 1*period\n",
    "\n",
    "num_sol_time = np.empty_like(dt_values, dtype=np.ndarray)\n",
    "\n",
    "\n",
    "for j, dt in enumerate(dt_values):\n",
    "\n",
    "    N = int(T/dt)\n",
    "    t = np.linspace(0, T, N)\n",
    "    \n",
    "    #initialize solution array\n",
    "    num_sol = np.zeros([N,2])\n",
    "    \n",
    "    \n",
    "    #Set intial conditions\n",
    "    num_sol[0,0] = x0\n",
    "    num_sol[0,1] = v0\n",
    "    \n",
    "    for i in range(N-1):\n",
    "        num_sol[i+1] = eulerstep(num_sol[i], springmass, dt)\n",
    "\n",
    "    num_sol_time[j] = num_sol.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous devrons calculer l'erreur avec la norme choisie, et nous écrivons une fonction pour cela. Cette fonction  comprend une ligne pour obtenir les valeurs de la solution analytique à l'instant nécessaire, puis il prend la différence avec la solution numérique pour calculer l'erreur."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_error(num_sol, T):\n",
    "    \n",
    "    x_an = x0 * np.cos(omega * T) # analytical solution at final time\n",
    "    \n",
    "    error =  np.abs(num_sol[-1,0] - x_an)\n",
    "    \n",
    "    return error"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Il ne reste plus qu'à appeler la fonction d'erreur avec nos valeurs choisies de $\\Delta t $ et à tracer les résultats. Une échelle logarithmique sur le tracé confirme une mise à l'échelle presque linéaire entre l'erreur et l'incrément de temps $\\Delta t $."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "error_values = np.empty_like(dt_values)\n",
    "\n",
    "for j in range(len(dt_values)):\n",
    "    \n",
    "    error_values[j] = get_error(num_sol_time[j], T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the solution errors with respect to the time incremetn\n",
    "fig = plt.figure(figsize=(8,8))\n",
    "\n",
    "plt.loglog(dt_values, error_values, 'ko-')  #log-log plot\n",
    "plt.loglog(dt_values, 10*dt_values, 'k:')\n",
    "plt.grid(True)                         #turn on grid lines\n",
    "plt.axis('equal')                      #make axes scale equally\n",
    "plt.xlabel('$\\Delta t$')\n",
    "plt.ylabel('Error')\n",
    "plt.title('Convergence of the Euler method (dotted line: slope 1)\\n');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Que voyez-vous dans le graphique de l'erreur en fonction de $\\Delta t $? \n",
    "\n",
    "Cela ressemble à une ligne droite, avec une pente proche de 1. Sur un tracé de convergence log-log, une pente de 1 indique que nous avons une méthode du premier ordre: l'erreur se met à l'échelle comme $ {\\mathcal O} (\\Delta t ) $ - en utilisant la notation \"big-O\". Cela signifie que l'erreur est proportionnelle à l'incrément de temps: $ e \\propto \\Delta t. $"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Amélioration de la précision\n",
    "\n",
    "Une autre amélioration de la méthode d'Euler est obtenue en faisant avancer la solution numérique au milieu d'un intervalle de temps, en y calculant les dérivées, puis en revenant en arrière et en mettant à jour l'état du système en utilisant les dérivées du point médian. C'est ce qu'on appelle une méthode de Runge Kutta 2.\n",
    "\n",
    "Si nous écrivons la forme vectorielle de l'équation différentielle comme:\n",
    "\\begin{equation}\n",
    "\\dot{\\mathbf{x}} = f(\\mathbf{x}),\n",
    "\\end{equation}\n",
    "\n",
    "alors la méthode numérique de Runge Kutta 2 s'écrit:\n",
    "\\begin{align}\n",
    "\\mathbf{x}_{n+1/2}   & = \\mathbf{x}_n + \\frac{\\Delta t}{2} f(\\mathbf{x}_n) \\\\\n",
    "\\mathbf{x}_{n+1} & = \\mathbf{x}_n + \\Delta t \\,\\, f(\\mathbf{x}_{n+1/2}).\n",
    "\\end{align}\n",
    "Nous pouvons maintenant écrire une fonction Python pour mettre à jour l'état en utilisant cette méthode. C'est équivalent à une méthode dite du second ordre Runge-Kutta, donc nous l'appelons `rk2_step ()`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rk2_step(state, rhs, dt):\n",
    "    '''Update a state to the next time increment using modified Euler's method.\n",
    "    \n",
    "    Arguments\n",
    "    ---------\n",
    "    state : array of dependent variables\n",
    "    rhs   : function that computes the RHS of the DiffEq\n",
    "    dt    : float, time increment\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    next_state : array, updated after one time increment'''\n",
    "    \n",
    "    mid_state = state + rhs(state) * dt*0.5    \n",
    "    next_state = state + rhs(mid_state)*dt\n",
    " \n",
    "    return next_state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Voyons comment cela fonctionne avec notre modèle de masse à ressort."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt_values = np.array([period/50, period/100, period/200,period/400])\n",
    "T = 1*period\n",
    "\n",
    "num_sol_time = np.empty_like(dt_values, dtype=np.ndarray)\n",
    "\n",
    "\n",
    "for j, dt in enumerate(dt_values):\n",
    "\n",
    "    N = int(T/dt)\n",
    "    t = np.linspace(0, T, N)\n",
    "    \n",
    "    #initialize solution array\n",
    "    num_sol = np.zeros([N,2])\n",
    "    \n",
    "    \n",
    "    #Set intial conditions\n",
    "    num_sol[0,0] = x0\n",
    "    num_sol[0,1] = v0\n",
    "    \n",
    "    for i in range(N-1):\n",
    "        num_sol[i+1] = rk2_step(num_sol[i], springmass, dt)\n",
    "\n",
    "    num_sol_time[j] = num_sol.copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "error_values = np.empty_like(dt_values)\n",
    "\n",
    "for j, dt in enumerate(dt_values):\n",
    "    \n",
    "    error_values[j] = get_error(num_sol_time[j], dt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot of convergence for modified Euler's\n",
    "fig = plt.figure(figsize=(10,10))\n",
    "\n",
    "plt.loglog(dt_values, error_values, 'ko-')\n",
    "plt.loglog(dt_values, 5*dt_values**2, 'k:')\n",
    "plt.grid(True)\n",
    "plt.axis('equal')\n",
    "plt.xlabel('$\\Delta t$')\n",
    "plt.ylabel('Error')\n",
    "plt.title('Convergence of modified Euler\\'s method (dotted line: slope 2)\\n');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La courbe de convergence, dans ce cas, ressemble à une droite de pente 2. La méthode proposée est précise au second ordre:\n",
    "le calcul des dérivées (pente) au milieu de l'intervalle de temps, au lieu du point de départ, a pour effet d'augmenter la précision d'un ordre!\n",
    "\n",
    "Utiliser les dérivées au milieu de l'intervalle de temps équivaut à utiliser la moyenne des dérivées à $ t $ et $ t + \\Delta t $:\n",
    "cela correspond à une méthode de Runge-Kutta  du second ordre, ou RK2, en abrégé.\n",
    "\n",
    "La combinaison de dérivés évalués à différents moments de l'intervalle de temps est la clé des méthodes de Runge-Kutta qui permettent d'obtenir des ordres de précision plus élevés."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bilan: qu'avez vous appris\n",
    "\n",
    "\n",
    "* forme vectorielle de l'équation différentielle masse-ressort\n",
    "* La méthode d'Euler produit une croissance d'amplitude non physique dans les systèmes oscillatoires\n",
    "* la méthode Euler-Cromer fixe la croissance d'amplitude (tout en étant toujours du premier ordre)\n",
    "* Euler-Cromer montre un décalage de phase après une longue simulation\n",
    "* un tracé de convergence confirme la précision du premier ordre de la méthode d'Euler\n",
    "* un graphique de convergence montre que la méthode d'Euler modifiée, utilisant les dérivées évaluées au milieu de l'intervalle de temps, est une méthode du second ordre"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "\n",
    "1. Linge S., Langtangen H.P. (2016) Solving Ordinary Differential Equations. In: Programming for Computations - Python. Texts in Computational Science and Engineering, vol 15. Springer, Cham, https://doi.org/10.1007/978-3-319-32428-9_4, open access and reusable under [CC-BY-NC](http://creativecommons.org/licenses/by-nc/4.0/) license.\n",
    "\n",
    "2. Cromer, A. (1981). Stable solutions using the Euler approximation. _American Journal of Physics_, 49(5), 455-459. https://doi.org/10.1119/1.12478\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "384px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
